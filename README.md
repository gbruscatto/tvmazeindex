# TVmaze Index
#### Gabriel Bruscatto

### Features
All features, mandatory and bonus, have been included.
##### Mandatory
- ✅ List all series by the paging scheme
- ✅ Allow users to search series by name
- ✅ Display the details of the show (name, poster, schedule, genres, summary, list of episodes separated by season)
- ✅ Display the details of episode (name, number, season, summary, image)

##### Optional
- ✅ Allow user to set a PIN number to secure the application
- ✅ For supported phones, allow user to use fingerprint to secure the application
- ✅ Allow user to save a series as a favorite
- ✅ Allow user to delete a series from favorites list
- ✅ Allow the user to browse their favorites series in alphabetical order, and click on one to see its details
- ✅ Create a people search by listing the name and image of the person
- ✅ After clicking on a person, the application should should show the details of that person (name, image, linked crew credits)
- ✅ Unit testing

##### Plus
- ✅ Show rating
- ✅ Show banner
- ✅ Episode rating
- ✅ Person gender
- ✅ Person birthday

## Apk 
You can find the apk file inside the `/apk` folder.
