package com.gb.tvmaze.index.data.usecase.settings

import com.gb.tvmaze.index.core.repository.settings.SettingsRepository
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@ExperimentalCoroutinesApi
internal class GetAuthPreferenceUseCaseImplTest {

    @ExperimentalCoroutinesApi
    @Test
    fun `get auth preference with success`() = runBlockingTest {
        val repository = mock<SettingsRepository> { }
        val useCase = GetAuthPreferenceUseCaseImpl(repository)
        useCase.invoke()
    }

}