package com.gb.tvmaze.index.data.usecase.favorite

import com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@ExperimentalCoroutinesApi
internal class GetFavoritesUseCaseImplTest {

    @ExperimentalCoroutinesApi
    @Test
    fun `get favorites with success`() = runBlockingTest {
        val repository = mock<FavoriteRepository> { }
        val useCase = GetFavoritesUseCaseImpl(repository)
        useCase.invoke()
    }

}