package com.gb.tvmaze.index.data.usecase.favorite

import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@ExperimentalCoroutinesApi
internal class SaveFavoriteUseCaseImplTest {

    private val show = com.gb.tvmaze.index.core.entity.Show(
        id = 123,
        name = "show name",
        summary = "show summary",
        rating = null,
        imagePreview = null,
        genres = emptyList(),
        status = com.gb.tvmaze.index.core.entity.ShowStatus.ENDED,
        schedule = null
    )

    @ExperimentalCoroutinesApi
    @Test
    fun `save favorite with success`() = runBlockingTest {
        val repository = mock<com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository> { }
        val useCase = SaveFavoriteUseCaseImpl(repository)
        useCase.invoke(show)
    }

}