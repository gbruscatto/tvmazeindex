package com.gb.tvmaze.index.data.usecase.show

import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@ExperimentalCoroutinesApi
internal class GetShowEpisodesUseCaseImplTest {

    private val dispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Test
    fun `get show episodes with success`() = runBlockingTest {
        val repository = mock<com.gb.tvmaze.index.core.repository.show.ShowRepository> { }
        val useCase = GetShowEpisodesUseCaseImpl(dispatcher, repository)
        useCase.invoke(1)
    }

}