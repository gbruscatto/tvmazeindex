package com.gb.tvmaze.index.data.usecase.people

import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@ExperimentalCoroutinesApi
internal class SearchPeopleUseCaseImplTest {

    private val dispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Test
    fun `search people with success`() = runBlockingTest {
        val repository = mock<com.gb.tvmaze.index.core.repository.people.PeopleRepository> { }
        val useCase = SearchPeopleUseCaseImpl(dispatcher, repository)
        useCase.invoke("any")
    }

}