package com.gb.tvmaze.index.data.usecase.favorite

import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@ExperimentalCoroutinesApi
internal class RemoveFavoriteUseCaseImplTest {

    @ExperimentalCoroutinesApi
    @Test
    fun `remove favorite with success`() = runBlockingTest {
        val repository = mock<com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository> { }
        val useCase = RemoveFavoriteUseCaseImpl(repository)
        useCase.invoke(1)
    }

}