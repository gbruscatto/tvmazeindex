package com.gb.tvmaze.index.data.repository.settings

import android.content.Context
import com.gb.tvmaze.index.core.repository.settings.SettingsRepository.Companion.AUTH_KEY
import com.gb.tvmaze.index.presentation.util.LoggingSharedPreferences
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import io.kotest.matchers.shouldBe
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
internal class SettingsRepositoryImplTest {

    @RelaxedMockK
    lateinit var loggingPreferences: LoggingSharedPreferences

    private val context: Context = mock {
        on { getSharedPreferences(any(), any()) }.thenReturn(mock {})
    }

    private val dispatcher = TestCoroutineDispatcher()
    private lateinit var repository: SettingsRepositoryImpl

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
        MockKAnnotations.init(this, relaxUnitFun = true)

        repository = SettingsRepositoryImpl(context).apply {
            settingsShared = loggingPreferences
        }

        addGetFavoritesStub()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getting boolean preference should return false`() {
        repository.getBoolean("any") shouldBe false
    }

    private fun addGetFavoritesStub() {
        coEvery { loggingPreferences.getBoolean(AUTH_KEY) } returns false
    }

}

