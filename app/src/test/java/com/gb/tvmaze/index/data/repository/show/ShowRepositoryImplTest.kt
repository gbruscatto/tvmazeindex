package com.gb.tvmaze.index.data.repository.show

import com.gb.tvmaze.index.data.repository.show.api.ShowApiService
import com.gb.tvmaze.index.data.repository.show.data.*
import com.gb.tvmaze.index.data.repository.show.mapper.toDomain
import io.kotest.matchers.shouldBe
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
internal class ShowRepositoryImplTest {

    private val dispatcher = TestCoroutineDispatcher()

    @RelaxedMockK
    lateinit var service: ShowApiService
    private lateinit var repository: ShowRepositoryImpl

    private val originalResolution = ShowImageResolutionOriginalData(
        url = "image.com/url"
    )

    private val resolutionsData = ShowImageResolutionsData(
        original = originalResolution
    )


    private val showEpisode = ShowEpisodeData(
        id = 123,
        name = "show name",
        summary = "show summary",
        season = 1,
        number = 1,
        imagePreview = null,
        rating = null
    )

    private val showImage = ShowImageData(
        type = "Background",
        showImageResolutions = resolutionsData
    )

    private val showData = ShowData(
        id = 123,
        name = "show name",
        summary = "show summary",
        rating = null,
        imagePreview = null,
        genres = null,
        status = "Ended",
        showScheduleData = null
    )

    private val searchData = SearchResultData(
        showData,
        personData = null
    )

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
        MockKAnnotations.init(this, relaxUnitFun = true)
        repository = ShowRepositoryImpl(
            dispatcher,
            service
        )

        addShowDetailsStub()
        addShowImagesStub()
        addShowEpisodesStub()
        addShowSearchStub()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getting show details should return an entity`() {
        dispatcher.runBlockingTest {
            repository.getDetails(1).collect {
                it shouldBe showData.toDomain()
            }
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getting show images should return a list of image entities`() {
        dispatcher.runBlockingTest {
            repository.getImages(1).collect {
                it shouldBe listOf(showImage).map { it.toDomain() }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getting episode list should return a list of episode entities`() {
        dispatcher.runBlockingTest {
            repository.getEpisodes(1).collect {
                it shouldBe listOf(showEpisode).map { it.toDomain() }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `searching should return a list of show entities`() {
        dispatcher.runBlockingTest {
            repository.search("any").collect {
                it shouldBe listOf(searchData).map { it.showData!!.toDomain() }
            }
        }
    }

    private fun addShowDetailsStub() {
        coEvery { service.getDetails(1) } returns showData
    }

    private fun addShowImagesStub() {
        coEvery { service.getImages(1) } returns listOf(showImage)
    }

    private fun addShowEpisodesStub() {
        coEvery { service.getEpisodes(1) } returns listOf(showEpisode)
    }

    private fun addShowSearchStub() {
        coEvery { service.search("any") } returns listOf(searchData)
    }
}

