package com.gb.tvmaze.index.data.usecase.show

import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@ExperimentalCoroutinesApi
internal class SearchShowsUseCaseImplTest {

    private val dispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Test
    fun `search shows with success`() = runBlockingTest {
        val repository = mock<com.gb.tvmaze.index.core.repository.show.ShowRepository> { }
        val useCase = SearchShowsUseCaseImpl(dispatcher, repository)
        useCase.invoke("any")
    }

}