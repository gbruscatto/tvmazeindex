package com.gb.tvmaze.index.data.repository.favorite

import android.content.Context
import com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository.Companion.FAVORITES
import com.gb.tvmaze.index.data.repository.show.data.*
import com.gb.tvmaze.index.presentation.util.LoggingSharedPreferences
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import io.kotest.matchers.shouldBe
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
internal class FavoriteRepositoryImplTest {

    @RelaxedMockK
    lateinit var loggingPreferences: LoggingSharedPreferences

    private val context: Context = mock {
        on { getSharedPreferences(any(), any()) }.thenReturn(mock {})
    }

    private val dispatcher = TestCoroutineDispatcher()
    private lateinit var repository: FavoriteRepositoryImpl

    private val gson = Gson()

    private val showData = ShowData(
        id = 123,
        name = "show name",
        summary = "show summary",
        rating = null,
        imagePreview = null,
        genres = emptyList(),
        status = null,
        showScheduleData = null
    )

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
        MockKAnnotations.init(this, relaxUnitFun = true)

        repository = FavoriteRepositoryImpl(context).apply {
            favoritesShared = loggingPreferences
        }

        addGetFavoritesStub()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getting favorites should return a list of show entities`() {
        repository.getFavorites() shouldBe
                gson.fromJson(
                    loggingPreferences.getString(FAVORITES),
                    Array<com.gb.tvmaze.index.core.entity.Show>::class.java
                ).toMutableList()
    }

    private fun addGetFavoritesStub() {
        coEvery { loggingPreferences.getString(FAVORITES) } returns gson.toJson(listOf(showData))
    }

}

