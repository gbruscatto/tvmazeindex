package com.gb.tvmaze.index.data.repository.people

import com.gb.tvmaze.index.data.repository.people.api.PeopleApiService
import com.gb.tvmaze.index.data.repository.people.mapper.toDomain
import com.gb.tvmaze.index.data.repository.show.data.*
import com.gb.tvmaze.index.data.repository.show.mapper.toDomain
import io.kotest.matchers.shouldBe
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
internal class PeopleRepositoryImplTest {

    private val dispatcher = TestCoroutineDispatcher()

    @RelaxedMockK
    lateinit var service: PeopleApiService
    private lateinit var repository: PeopleRepositoryImpl

    private val personData = PersonData(
        id = 123,
        name = "show name",
        birthday = "person birthday",
        gender = "Male",
        imagePreview = null
    )

    private val showData = ShowData(
        id = 123,
        name = "show name",
        summary = "show summary",
        rating = null,
        imagePreview = null,
        genres = null,
        status = null,
        showScheduleData = null
    )

    private val searchData = SearchResultData(
        showData = null,
        personData
    )

    private val embeddedData = CrewCreditsEmbeddedData(
        showData
    )

    private val crewCreditsData = CrewCreditsData(
        embeddedData
    )

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
        MockKAnnotations.init(this, relaxUnitFun = true)
        repository = PeopleRepositoryImpl(
            dispatcher,
            service
        )

        addPeopleSearchStub()
        addCrewCreditsStub()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `searching should return a list of person entities`() {
        dispatcher.runBlockingTest {
            repository.search("any").collect {
                it shouldBe listOf(searchData).map { it.personData!!.toDomain() }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `getting crew credits should return a list of show entities`() {
        dispatcher.runBlockingTest {
            repository.getCrewCredits(1).collect {
                it shouldBe listOf(crewCreditsData).map { it.embeddedData.showData.toDomain() }
            }
        }
    }

    private fun addPeopleSearchStub() {
        coEvery { service.search("any") } returns listOf(searchData)
    }

    private fun addCrewCreditsStub() {
        coEvery { service.getCrewCredits(1) } returns listOf(crewCreditsData)
    }
}

