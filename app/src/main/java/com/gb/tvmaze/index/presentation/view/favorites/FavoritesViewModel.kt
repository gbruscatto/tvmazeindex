package com.gb.tvmaze.index.presentation.view.favorites

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.usecase.favorite.GetFavoritesUseCase
import com.gb.tvmaze.index.core.usecase.favorite.RemoveFavoriteUseCase
import com.gb.tvmaze.index.core.usecase.favorite.SortFavoritesUseCase
import com.gb.tvmaze.index.presentation.view.favorites.FavoriteViewItem.Companion.showToFavoriteViewItem
import java.net.URL

class FavoritesViewModel(
    private val getFavoritesUseCase: GetFavoritesUseCase,
    private val removeFavoriteUseCase: RemoveFavoriteUseCase,
    private val sortUseCase: SortFavoritesUseCase
) : ViewModel() {

    val favorites = MutableLiveData<List<FavoriteViewItem>>()

    fun getFavorites() {
        favorites.value = getFavoritesUseCase.invoke().map {
            showToFavoriteViewItem(it)
        }
    }

    fun unfavoriteShow(id: Int) {
        removeFavoriteUseCase.invoke(id)
    }

    fun sortFavoritesAlphabetically() {
        favorites.value = sortUseCase.invoke().map {
            showToFavoriteViewItem(it)
        }
    }

}

class FavoriteViewItem(
    val id: Int,
    var name: String,
    var summary: String?,
    val imagePreview: URL?
) {
    companion object {
        fun showToFavoriteViewItem(show: Show) =
            FavoriteViewItem(
                show.id,
                show.name,
                show.summary,
                show.imagePreview
            )
    }
}