package com.gb.tvmaze.index.data.repository.show

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.entity.ShowEpisode
import com.gb.tvmaze.index.core.entity.ShowImage
import com.gb.tvmaze.index.core.repository.show.ShowRepository
import com.gb.tvmaze.index.data.repository.show.api.ShowApiService
import com.gb.tvmaze.index.data.repository.show.mapper.toDomain
import com.gb.tvmaze.index.data.repository.show.paging.ShowPagingSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map


class ShowRepositoryImpl(
    private val defaultDispatcher: CoroutineDispatcher,
    private val showApiService: ShowApiService
) : ShowRepository {

    companion object {
        private const val TAG = "ShowRepositoryImpl"
        private const val PAGE_SIZE = 250
    }

    override fun list(): Flow<PagingData<Show>> =
        Pager(
            config = PagingConfig(PAGE_SIZE, enablePlaceholders = false),
            pagingSourceFactory = { ShowPagingSource(showApiService) }
        ).flow.map {
            it.map { showData -> showData.toDomain() }
        }.flowOn(defaultDispatcher)

    override fun getDetails(id: Int): Flow<Show> =
        flow {
            emit(showApiService.getDetails(id).toDomain() )
        }.flowOn(defaultDispatcher)

    override fun getImages(id: Int): Flow<List<ShowImage>> =
        flow {
            emit(showApiService.getImages(id).map { it.toDomain() } )
        }.flowOn(defaultDispatcher)

    override fun getEpisodes(id: Int): Flow<List<ShowEpisode>> =
        flow {
            emit(showApiService.getEpisodes(id).map { it.toDomain() } )
        }.flowOn(defaultDispatcher)

    override fun search(query: String): Flow<List<Show>> =
        flow {
            emit(showApiService.search(query).map { it.showData!!.toDomain() } )
        }.flowOn(defaultDispatcher)

}