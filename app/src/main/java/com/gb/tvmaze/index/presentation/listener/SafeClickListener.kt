package com.gb.tvmaze.index.presentation.listener

import android.os.SystemClock
import android.view.View

class SafeClickListener(
    private val clickListener: (View) -> Unit, private var interval: Int = 1000
) : View.OnClickListener {

    private var lastTimeClicked: Long = 0

    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - lastTimeClicked < interval) return

        lastTimeClicked = SystemClock.elapsedRealtime()
        clickListener(v)
    }
}

fun View.setSafeClickListener(clickListener: (View) -> Unit) =
    this.setOnClickListener(SafeClickListener(clickListener))