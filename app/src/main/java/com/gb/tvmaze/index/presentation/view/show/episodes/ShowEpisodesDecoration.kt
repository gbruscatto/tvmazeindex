package com.gb.tvmaze.index.presentation.view.show.episodes

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ShowEpisodesDecoration (
    private val marginBetweenItems: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        with(outRect) {
            if (parent.getChildAdapterPosition(view) == FIRST_POSITION) {
                top = 0
                return
            }
            top = marginBetweenItems
        }
    }

    companion object {
        private const val FIRST_POSITION = 0
    }
}
