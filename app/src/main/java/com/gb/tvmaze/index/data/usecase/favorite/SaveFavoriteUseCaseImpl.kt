package com.gb.tvmaze.index.data.usecase.favorite

import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository
import com.gb.tvmaze.index.core.usecase.favorite.SaveFavoriteUseCase

class SaveFavoriteUseCaseImpl(
    private val favoriteRepository: FavoriteRepository
) : SaveFavoriteUseCase {

    override fun invoke(show: Show) =
        favoriteRepository.saveFavorite(show)

}