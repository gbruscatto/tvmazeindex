package com.gb.tvmaze.index.data.usecase.favorite

import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository
import com.gb.tvmaze.index.core.usecase.favorite.SortFavoritesUseCase

class SortFavoritesUseCaseImpl(
    private val favoriteRepository: FavoriteRepository
) : SortFavoritesUseCase {

    override fun invoke(): List<Show> =
        favoriteRepository.sortFavorites()

}