package com.gb.tvmaze.index.data.usecase.people

import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.repository.people.PeopleRepository
import com.gb.tvmaze.index.core.usecase.people.GetCrewCreditsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

class GetCrewCreditsUseCaseImpl(
    private val defaultDispatcher: CoroutineDispatcher,
    private val peopleRepository: PeopleRepository
) : GetCrewCreditsUseCase {

    override fun invoke(id: Int): Flow<List<Show>> =
        peopleRepository.getCrewCredits(id).flowOn(defaultDispatcher)

}