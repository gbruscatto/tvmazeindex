package com.gb.tvmaze.index.core.entity

data class ShowSchedule(
    var time: String,
    var days: List<String>
)