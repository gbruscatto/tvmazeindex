package com.gb.tvmaze.index.core.usecase.settings

interface ChangeAuthPreferenceUseCase {

    fun invoke(value: Boolean)

}