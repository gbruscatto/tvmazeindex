package com.gb.tvmaze.index.data.repository.show.data

import com.google.gson.annotations.SerializedName

data class CrewCreditsData(

    @SerializedName("_embedded")
    var embeddedData: CrewCreditsEmbeddedData

)
