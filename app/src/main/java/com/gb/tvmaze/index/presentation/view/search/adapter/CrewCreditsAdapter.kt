package com.gb.tvmaze.index.presentation.view.search.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.bumptech.glide.Glide
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.ComponentCrewCreditsItemBinding
import com.gb.tvmaze.index.presentation.extension.toUri
import com.gb.tvmaze.index.presentation.view.search.SearchViewItem

class CrewCreditsAdapter(
    private var items: List<SearchViewItem.ShowItem>
) : Adapter<CrewCreditsAdapter.CrewCreditsViewHolder>() {

    var onShowClick: ((Int) -> Unit)? = null

    fun updateData(items: List<SearchViewItem.ShowItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CrewCreditsViewHolder {
        ComponentCrewCreditsItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ).apply { return CrewCreditsViewHolder(this) }
    }

    override fun onBindViewHolder(holder: CrewCreditsViewHolder, position: Int) {
        holder.bind(items[position])
        setupShowClickListener(holder, items[position])
    }

    private fun setupShowClickListener(holder: CrewCreditsViewHolder, show: SearchViewItem.ShowItem) {
        holder.binding.container.setOnClickListener {
            onShowClick?.invoke(show.id)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class CrewCreditsViewHolder(
        val binding: ComponentCrewCreditsItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SearchViewItem.ShowItem) {
            with(binding) {
                posterPlaceholder.visibility = View.VISIBLE
                item.imagePreview?.let {
                    Glide.with(root)
                        .load(it.toUri())
                        .placeholder(R.drawable.layout_crew_credits_placeholder)
                        .centerInside()
                        .into(poster)
                    posterPlaceholder.visibility = View.GONE
                }
            }
        }

    }

}
