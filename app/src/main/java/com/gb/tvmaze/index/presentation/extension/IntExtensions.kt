package com.gb.tvmaze.index.presentation.extension

@JvmOverloads
    fun Int.toStringTwoDigits(): String =
        if(this in 0..9) { "0$this" } else { this.toString() }