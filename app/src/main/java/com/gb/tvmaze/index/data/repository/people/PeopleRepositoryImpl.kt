package com.gb.tvmaze.index.data.repository.people

import com.gb.tvmaze.index.core.entity.Person
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.repository.people.PeopleRepository
import com.gb.tvmaze.index.data.repository.people.api.PeopleApiService
import com.gb.tvmaze.index.data.repository.people.mapper.toDomain
import com.gb.tvmaze.index.data.repository.show.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class PeopleRepositoryImpl(
    private val defaultDispatcher: CoroutineDispatcher,
    private val peopleApiService: PeopleApiService
) : PeopleRepository {

    override fun search(query: String): Flow<List<Person>> =
        flow {
            emit(peopleApiService.search(query).map { it.personData!!.toDomain() } )
        }.flowOn(defaultDispatcher)

    override fun getCrewCredits(id: Int): Flow<List<Show>> =
        flow {
            emit(peopleApiService.getCrewCredits(id).map { it.toDomain() } )
        }.flowOn(defaultDispatcher)

}