package com.gb.tvmaze.index.data.repository.show.mapper

import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.entity.ShowStatus
import com.gb.tvmaze.index.data.repository.show.data.ShowData
import java.net.URL

private const val RUNNING = "Running"
private const val ENDED = "Ended"

fun ShowData.toDomain() = Show(
    id,
    name,
    summary ?: "",
    rating?.average,
    imagePreview?.medium.toURL(),
    genres ?: emptyList(),
    status = when (status) {
        RUNNING -> ShowStatus.RUNNING
        ENDED -> ShowStatus.ENDED
        else -> ShowStatus.UNKNOWN
    },
    showScheduleData?.toDomain()
)

private fun String?.toURL(): URL? =
    this?.let { runCatching { URL(it) }.getOrNull() }
