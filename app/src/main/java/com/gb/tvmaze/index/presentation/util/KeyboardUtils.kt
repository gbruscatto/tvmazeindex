package com.gb.tvmaze.index.presentation.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

class KeyboardUtils(context: Context) {

    private val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

    fun hideKeyboard(view: View) {
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}