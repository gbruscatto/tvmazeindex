package com.gb.tvmaze.index.presentation.alert

sealed class ProgressState {
    object Nothing : ProgressState()
    object Loading : ProgressState()
}