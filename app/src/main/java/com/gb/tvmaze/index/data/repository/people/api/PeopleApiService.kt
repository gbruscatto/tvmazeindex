package com.gb.tvmaze.index.data.repository.people.api

import com.gb.tvmaze.index.data.repository.show.data.CrewCreditsData
import com.gb.tvmaze.index.data.repository.show.data.SearchResultData
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PeopleApiService {

    @GET("/search/people")
    suspend fun search(
        @Query("q") query: String
    ): List<SearchResultData>

    @GET("/people/{id}/crewcredits?embed=show")
    suspend fun getCrewCredits(
        @Path("id") id: Int
    ): List<CrewCreditsData>

}