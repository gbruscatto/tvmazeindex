package com.gb.tvmaze.index.data.repository.show.mapper

import com.gb.tvmaze.index.core.entity.ShowSchedule
import com.gb.tvmaze.index.data.repository.show.data.ShowScheduleData

fun ShowScheduleData.toDomain() = ShowSchedule(
    time,
    days ?: emptyList()
)