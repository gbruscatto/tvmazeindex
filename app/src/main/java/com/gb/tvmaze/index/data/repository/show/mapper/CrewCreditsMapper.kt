package com.gb.tvmaze.index.data.repository.show.mapper

import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.entity.ShowStatus
import com.gb.tvmaze.index.data.repository.show.data.CrewCreditsData
import java.net.URL

fun CrewCreditsData.toDomain() = Show(
    embeddedData.showData.id,
    embeddedData.showData.name,
    embeddedData.showData.summary ?: "",
    null,
    embeddedData.showData.imagePreview?.medium.toURL(),
    embeddedData.showData.genres ?: emptyList(),
    ShowStatus.UNKNOWN,
    null
)

private fun String?.toURL(): URL? =
    this?.let { runCatching { URL(it) }.getOrNull() }
