package com.gb.tvmaze.index.di.modules

import com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository
import com.gb.tvmaze.index.core.repository.people.PeopleRepository
import com.gb.tvmaze.index.core.repository.settings.SettingsRepository
import com.gb.tvmaze.index.core.repository.show.ShowRepository
import com.gb.tvmaze.index.data.infraestructure.restapi.retrofit.RetrofitFactory
import com.gb.tvmaze.index.data.repository.favorite.FavoriteRepositoryImpl
import com.gb.tvmaze.index.data.repository.people.PeopleRepositoryImpl
import com.gb.tvmaze.index.data.repository.people.api.PeopleApiService
import com.gb.tvmaze.index.data.repository.settings.SettingsRepositoryImpl
import com.gb.tvmaze.index.data.repository.show.ShowRepositoryImpl
import com.gb.tvmaze.index.data.repository.show.api.ShowApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.dsl.module

@FlowPreview
@ExperimentalCoroutinesApi
val dataModule = module {
    single<ShowRepository> { ShowRepositoryImpl(Dispatchers.IO, get()) }
    single<PeopleRepository> { PeopleRepositoryImpl(Dispatchers.IO, get()) }
    single<FavoriteRepository> { FavoriteRepositoryImpl(get()) }
    single<SettingsRepository> { SettingsRepositoryImpl(get()) }

    single<ShowApiService> { RetrofitFactory.retrofit.create(ShowApiService::class.java) }
    single<PeopleApiService> { RetrofitFactory.retrofit.create(PeopleApiService::class.java) }
}
