package com.gb.tvmaze.index.presentation.view.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.bumptech.glide.Glide
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.ComponentSearchShowItemBinding
import com.gb.tvmaze.index.presentation.extension.spannableFromHtml
import com.gb.tvmaze.index.presentation.extension.toUri
import com.gb.tvmaze.index.presentation.view.search.SearchViewItem

class ShowSearchResultAdapter(
    private var items: List<SearchViewItem.ShowItem>
) : Adapter<ShowSearchResultAdapter.ShowViewHolder>() {

    var onShowClick: ((Int) -> Unit)? = null

    fun updateData(items: List<SearchViewItem.ShowItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder {
        ComponentSearchShowItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ).apply { return ShowViewHolder(this) }
    }

    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        holder.bind(items[position])
        setupShowClickListener(holder, items[position])
    }

    private fun setupShowClickListener(holder: ShowViewHolder, show: SearchViewItem.ShowItem) {
        holder.binding.container.setOnClickListener {
            onShowClick?.invoke(show.id)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ShowViewHolder(
        val binding: ComponentSearchShowItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SearchViewItem.ShowItem) {
            with(binding) {
                name.text = item.name
                summary.text = item.summary?.spannableFromHtml()
                item.imagePreview?.let {
                    Glide.with(root)
                        .load(it.toUri())
                        .placeholder(R.drawable.layout_search_placeholder)
                        .centerInside()
                        .into(poster)
                }

            }
        }
    }

}
