package com.gb.tvmaze.index.presentation.view.show

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.core.entity.*
import com.gb.tvmaze.index.databinding.FragmentShowDetailsBinding
import com.gb.tvmaze.index.presentation.alert.ProgressState
import com.gb.tvmaze.index.presentation.base.BaseFragment
import com.gb.tvmaze.index.presentation.extension.spannableFromHtml
import com.gb.tvmaze.index.presentation.extension.toDp
import com.gb.tvmaze.index.presentation.extension.toUri
import com.gb.tvmaze.index.presentation.listener.setSafeClickListener
import com.gb.tvmaze.index.presentation.view.show.episodes.ShowEpisodesAdapter
import com.gb.tvmaze.index.presentation.view.show.episodes.ShowEpisodesDecoration
import com.gb.tvmaze.index.presentation.view.show.episodes.dialog.ShowEpisodeBottomSheetDialog
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.error.MissingPropertyException
import java.net.URL
import java.util.*

class ShowDetailsFragment : BaseFragment() {

    companion object {
        const val TAG = "ShowDetailsFragment"
        const val ARGUMENT_SHOW_ID = "ARGUMENT_SHOW_ID"
    }

    private val viewModel by viewModel<ShowDetailsViewModel>()

    private var _binding: FragmentShowDetailsBinding? = null
    private val binding get() = _binding!!

    private var showId: Int = -1
    private var episodesAdapter: ShowEpisodesAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        FragmentShowDetailsBinding
            .inflate(inflater, container, false).apply {
                _binding = this
                return this.root
            }
    }

    private fun isShowIdValid(): Boolean {
        return arguments?.let {
            showId = it.getInt(ARGUMENT_SHOW_ID, -1)
            true
        }?: run {
            throw MissingPropertyException(
                "You must supply a show id to use this fragment."
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(!isShowIdValid()) { return }

        initViews()
        observeViewModel()
        viewModel.getDetails(showId)
    }

    override fun observeViewModel() {
        viewModel.loading.observe(viewLifecycleOwner, {
            binding.loadingDetails.visibility =
                if(it is ProgressState.Loading) { View.VISIBLE }
                else { View.GONE }
        })

        viewModel.error.observe(viewLifecycleOwner, { error ->
            when(error) {
                ShowDetailsError.DETAILS_ERROR -> {
                    binding.errorLayout.container.visibility = View.VISIBLE
                }

                ShowDetailsError.EPISODES_ERROR -> {
                    binding.episodesEmptyPlaceholder.visibility = View.VISIBLE
                }
                else -> {}
            }
        })

        viewModel.show.observe(viewLifecycleOwner, {
            binding.errorLayout.container.visibility = View.GONE
            setDetails(it)
            configFavoriteButton()
            viewModel.getImages(showId)
            viewModel.getEpisodes(showId)
        })

        viewModel.images.observe(viewLifecycleOwner, { images ->
            images.forEach { currentImage ->
                if(currentImage.type == ShowImageType.BACKGROUND) {
                    setBackground(currentImage)
                    return@forEach
                }
            }
        })

        viewModel.episodes.observe(viewLifecycleOwner, {
            if(it.isEmpty()) {
                binding.episodesEmptyPlaceholder.visibility = View.VISIBLE
            } else {
                binding.episodesEmptyPlaceholder.visibility = View.GONE
                setEpisodes(it)
            }


        })
    }

    override fun initViews() {
        binding.favorite.setSafeClickListener {
            if(!viewModel.favorited) { favoriteShow() }
            else { unfavoriteShow() }
        }
    }

    private fun favoriteShow() {
        viewModel.favorited = true
        binding.favorite.setImageResource(R.drawable.ic_heart_filled)
        binding.favorite.setColorFilter(
            ContextCompat.getColor(requireContext(), R.color.red_f0),
            android.graphics.PorterDuff.Mode.SRC_IN
        )
        viewModel.favoriteShow()
        Toast.makeText(
            requireContext(),
            R.string.added_to_favorites,
            Toast.LENGTH_LONG
        ).show()
    }

    private fun unfavoriteShow() {
        viewModel.favorited = false
        binding.favorite.setImageResource(R.drawable.ic_heart_unfilled)
        binding.favorite.setColorFilter(
            ContextCompat.getColor(requireContext(), R.color.grey_a6),
            android.graphics.PorterDuff.Mode.SRC_IN
        )
        viewModel.unfavoriteShow()
        Toast.makeText(
            requireContext(),
            R.string.removed_from_favorites,
            Toast.LENGTH_LONG
        ).show()
    }

    private fun configFavoriteButton() {
        if(viewModel.isFavorite()) {
            viewModel.favorited = true
            binding.favorite.setImageResource(R.drawable.ic_heart_filled)
            binding.favorite.setColorFilter(
                ContextCompat.getColor(requireContext(), R.color.red_f0),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
        }
    }

    private fun setDetails(show: Show) {
        setNameAndSummary(show.name, show.summary)
        setGenres(show.genres)
        setStatus(show.status)
        setSchedule(show.schedule)
        setRating(show.rating)
        setPoster(show.imagePreview)
    }

    private fun setNameAndSummary(name: String, summary: String) {
        binding.name.text = name
        binding.summary.text = summary.spannableFromHtml()
    }

    private fun setGenres(genres: List<String>) {
        genres.forEach {
            var genresText = binding.genres.text.toString()
            if(genresText.isNotEmpty()) { genresText += ", " }
            genresText += it
            binding.genres.text = genresText
        }
    }

    private fun setStatus(value: ShowStatus) {
        binding.status.text = value.toString().lowercase()
            .replaceFirstChar { it.titlecase(Locale.getDefault()) }
    }

    private fun setSchedule(schedule: ShowSchedule?) {
        var time = schedule?.time
        var days = ""

        with(binding) {
            schedule?.days?.let {
                it.forEachIndexed { index, day ->
                    days += day
                    if(index < it.size-1) { days += ", " }
                }
            }

            time += ": $days"
            airsOn.text = time
        }
    }

    private fun setRating(value: Double?) {
        with(binding) {
            value?.let {
                rating.text = it.toString()
            }?: run {
                ratingStar.visibility = View.GONE
                rating.visibility = View.GONE
            }
        }
    }

    private fun setPoster(image: URL?) {
        with(binding) {
            Glide.with(root)
                .load(image?.toUri())
                .placeholder(R.drawable.layout_show_poster_placeholder)
                .centerCrop()
                .into(poster)
        }
    }

    private fun setBackground(it: ShowImage) {
        with(binding) {
            Glide.with(root)
                .load(it.url?.toUri())
                .placeholder(R.drawable.layout_show_background_placeholder)
                .centerCrop()
                .into(background)
        }
    }

    private fun setEpisodes(episodes: List<EpisodesViewItem>) {
        with(binding) {
            episodesAdapter = ShowEpisodesAdapter(episodes)
            recyclerViewEpisodes.adapter = episodesAdapter

            val marginBetweenItems = 24f.toDp().toInt()
            binding.recyclerViewEpisodes.addItemDecoration(
                ShowEpisodesDecoration(marginBetweenItems)
            )

            episodesAdapter?.onEpisodeClick = { episode ->
                ShowEpisodeBottomSheetDialog(episode).show(parentFragmentManager, null)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}