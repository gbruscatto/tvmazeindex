package com.gb.tvmaze.index.core.usecase.show

import com.gb.tvmaze.index.core.entity.Show
import kotlinx.coroutines.flow.Flow

interface GetShowDetailsUseCase {

    fun invoke(id: Int): Flow<Show>

}