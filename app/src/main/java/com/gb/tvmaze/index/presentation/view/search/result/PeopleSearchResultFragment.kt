package com.gb.tvmaze.index.presentation.view.search.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gb.tvmaze.index.databinding.FragmentSearchResultPeopleBinding
import com.gb.tvmaze.index.presentation.base.BaseFragment
import com.gb.tvmaze.index.presentation.extension.toDp
import com.gb.tvmaze.index.presentation.view.main.MainActivity
import com.gb.tvmaze.index.presentation.view.search.SearchViewModel
import com.gb.tvmaze.index.presentation.view.search.adapter.PeopleSearchResultAdapter
import com.gb.tvmaze.index.presentation.view.search.dialog.PersonBottomSheetDialog
import com.gb.tvmaze.index.presentation.view.show.episodes.ShowEpisodesDecoration
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class PeopleSearchResultFragment : BaseFragment() {

    private val viewModel by sharedViewModel<SearchViewModel>()

    private var _binding: FragmentSearchResultPeopleBinding? = null
    private val binding get() = _binding!!

    private var adapter: PeopleSearchResultAdapter? = null
    private var lastQuery = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        FragmentSearchResultPeopleBinding
            .inflate(inflater, container, false).apply {
                _binding = this
                return this.root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initViews()
        observeViewModel()
    }

    private fun initAdapter() {
        adapter = PeopleSearchResultAdapter(emptyList())
        adapter?.onPersonClick = { person ->
            viewModel.currentPerson = person
            viewModel.getCrewCredits(person.id)
        }
    }

    override fun observeViewModel() {
        viewModel.people.observe(viewLifecycleOwner, {
            if(it.isNotEmpty()) {
                binding.emptyPlaceholder.containerEmpty.visibility = View.GONE
                adapter?.updateData(it)
            } else {
                binding.emptyPlaceholder.containerEmpty.visibility = View.VISIBLE
            }
        })

        viewModel.crewCredits.observe(viewLifecycleOwner, { crewCredits ->
            viewModel.currentPerson?.let {
                val personDialog = PersonBottomSheetDialog(it, crewCredits)
                personDialog.show(parentFragmentManager, null)
                personDialog.onShowClick = { id ->
                    personDialog.dismiss()
                    (activity as MainActivity).goToShowDetails(id)
                }
            }
        })
    }

    override fun initViews() {
        with(binding) {
            recyclerViewSearch.adapter = adapter
            val marginBetweenItems = 24f.toDp().toInt()
            binding.recyclerViewSearch.addItemDecoration(
                ShowEpisodesDecoration(marginBetweenItems)
            )
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.query.let {
            if(lastQuery != it) search(it)
        }
    }

    fun search(query: String) {
        binding.emptyPlaceholder.containerEmpty.visibility = View.GONE
        lastQuery = query
        viewModel.searchPeople(query)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter = null
        _binding = null
    }
}