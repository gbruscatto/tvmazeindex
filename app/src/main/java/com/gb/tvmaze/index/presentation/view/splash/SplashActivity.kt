package com.gb.tvmaze.index.presentation.view.splash

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.ActivitySplashBinding
import com.gb.tvmaze.index.presentation.listener.setSafeClickListener
import com.gb.tvmaze.index.presentation.view.main.MainActivity
import com.gb.tvmaze.index.presentation.view.settings.SettingsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.lang.Exception

class SplashActivity : AppCompatActivity() {

    companion object {
        const val TAG = "SplashActivity"
        private const val ANIM_DURATION: Long = 2000
    }

    private lateinit var binding: ActivitySplashBinding
    private val viewModel by viewModel<SettingsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupTryAgainButton()

        val fadeIn: Animation = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator()
        fadeIn.duration = ANIM_DURATION
        binding.appLogo.startAnimation(fadeIn)
        Handler(Looper.getMainLooper()).postDelayed({
            if(viewModel.isAuthModeActive()) { auth() }
            else { goToMainActivity() }
        }, ANIM_DURATION)
    }

    private fun setupTryAgainButton() {
        binding.tryAgain.setSafeClickListener {
            auth()
        }
    }

    private fun auth() {
        try {
            if(isDeviceAnEmulator()) {
                Toast.makeText(this,
                    getString(R.string.auth_emulator_not_available), Toast.LENGTH_SHORT)
                    .show()
                goToMainActivity()
            } else {
                val canAuthenticate = BiometricManager.from(this).canAuthenticate()
                if (canAuthenticate == BiometricManager.BIOMETRIC_SUCCESS) {
                    createBiometricPrompt().authenticate(createPromptInfo())
                } else {
                    goToMainActivity()
                }
            }
        }catch (e: Exception) {
            goToMainActivity()
        }
    }

    private fun goToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun createPromptInfo(): BiometricPrompt.PromptInfo {
        return BiometricPrompt.PromptInfo.Builder()
            .setTitle("My App's Authentication")
            .setSubtitle("Please login to get access")
            .setDescription("My App is using Android biometric authentication")
            .setDeviceCredentialAllowed(true)
            .build()
    }

    private fun createBiometricPrompt(): BiometricPrompt {
        val executor = ContextCompat.getMainExecutor(this)

        val callback = object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                binding.authLayout.visibility = View.VISIBLE
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                binding.authLayout.visibility = View.GONE
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                Toast.makeText(this@SplashActivity,
                        getString(R.string.auth_succeeded), Toast.LENGTH_SHORT)
                        .show()
                goToMainActivity()
            }
        }

        return BiometricPrompt(this, executor, callback)
    }

    private fun isDeviceAnEmulator() =
        Build.FINGERPRINT.contains("generic")

}

