package com.gb.tvmaze.index.core.usecase.favorite

interface RemoveFavoriteUseCase {

    fun invoke(id: Int)

}