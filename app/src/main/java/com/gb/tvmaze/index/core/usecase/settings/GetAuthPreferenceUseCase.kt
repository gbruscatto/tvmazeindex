package com.gb.tvmaze.index.core.usecase.settings

interface GetAuthPreferenceUseCase {

    fun invoke(): Boolean

}