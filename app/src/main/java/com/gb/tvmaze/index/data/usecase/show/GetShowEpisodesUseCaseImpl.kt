package com.gb.tvmaze.index.data.usecase.show

import com.gb.tvmaze.index.core.entity.ShowEpisode
import com.gb.tvmaze.index.core.repository.show.ShowRepository
import com.gb.tvmaze.index.core.usecase.show.GetShowEpisodesUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

class GetShowEpisodesUseCaseImpl(
    private val defaultDispatcher: CoroutineDispatcher,
    private val showRepository: ShowRepository
) : GetShowEpisodesUseCase {

    override fun invoke(id: Int): Flow<List<ShowEpisode>> =
        showRepository.getEpisodes(id).flowOn(defaultDispatcher)

}