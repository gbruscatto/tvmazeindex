package com.gb.tvmaze.index.data.repository.show.data

import com.google.gson.annotations.SerializedName

data class ShowImageData(

    @SerializedName("type")
    var type: String?,

    @SerializedName("resolutions")
    var showImageResolutions: ShowImageResolutionsData

)
