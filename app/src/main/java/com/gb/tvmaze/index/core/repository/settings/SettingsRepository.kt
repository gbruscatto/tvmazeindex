package com.gb.tvmaze.index.core.repository.settings

interface SettingsRepository {

    fun saveBoolean(key: String, value: Boolean)

    fun getBoolean(key: String): Boolean

    companion object {
        const val SETTINGS = "settings"
        const val AUTH_KEY = "auth_key"
    }
}