package com.gb.tvmaze.index.data.usecase.favorite

import com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository
import com.gb.tvmaze.index.core.usecase.favorite.RemoveFavoriteUseCase

class RemoveFavoriteUseCaseImpl(
    private val favoriteRepository: FavoriteRepository
) : RemoveFavoriteUseCase {

    override fun invoke(id: Int) =
        favoriteRepository.removeFavorite(id)

}