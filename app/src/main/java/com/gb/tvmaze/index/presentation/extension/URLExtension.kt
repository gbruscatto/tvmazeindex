package com.gb.tvmaze.index.presentation.extension

import android.net.Uri
import java.net.URL

fun URL.toUri() : Uri = Uri.parse(this.toURI().toString())