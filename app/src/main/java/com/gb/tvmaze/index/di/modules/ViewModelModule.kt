package com.gb.tvmaze.index.di.modules

import com.gb.tvmaze.index.presentation.view.favorites.FavoritesViewModel
import com.gb.tvmaze.index.presentation.view.index.ShowIndexViewModel
import com.gb.tvmaze.index.presentation.view.search.SearchViewModel
import com.gb.tvmaze.index.presentation.view.settings.SettingsViewModel
import com.gb.tvmaze.index.presentation.view.show.ShowDetailsViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

@FlowPreview
@ExperimentalCoroutinesApi
val viewModelModule = module {
    viewModel { ShowIndexViewModel(get()) }
    viewModel { ShowDetailsViewModel(get(), get(), get(), get(), get(), get()) }
    viewModel { SearchViewModel(get(), get(), get()) }
    viewModel { FavoritesViewModel(get(), get(), get()) }
    viewModel { SettingsViewModel(get(), get()) }
}
