package com.gb.tvmaze.index.core.entity

import java.net.URL

data class ShowImage(
    var type: ShowImageType,
    var url: URL?
)

enum class ShowImageType {
    POSTER,
    BANNER,
    BACKGROUND,
    TYPOGRAPHY,
    UNKNOWN
}