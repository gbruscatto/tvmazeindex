package com.gb.tvmaze.index.presentation.extension

import android.os.Build
import android.text.Html
import android.text.SpannableStringBuilder

fun String.spannableFromHtml(): SpannableStringBuilder =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY) as SpannableStringBuilder
    } else {
        Html.fromHtml(this) as SpannableStringBuilder
    }