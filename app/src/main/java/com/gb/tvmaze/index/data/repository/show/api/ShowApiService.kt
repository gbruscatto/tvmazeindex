package com.gb.tvmaze.index.data.repository.show.api

import com.gb.tvmaze.index.data.repository.show.data.SearchResultData
import com.gb.tvmaze.index.data.repository.show.data.ShowData
import com.gb.tvmaze.index.data.repository.show.data.ShowEpisodeData
import com.gb.tvmaze.index.data.repository.show.data.ShowImageData
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ShowApiService {

    @GET("shows")
    suspend fun list(
        @Query("page") page: Int
    ): List<ShowData>

    @GET("shows/{id}")
    suspend fun getDetails(
        @Path("id") id: Int
    ): ShowData

    @GET("shows/{id}/images")
    suspend fun getImages(
        @Path("id") id: Int
    ): List<ShowImageData>

    @GET("shows/{id}/episodes")
    suspend fun getEpisodes(
        @Path("id") id: Int
    ): List<ShowEpisodeData>

    @GET("/search/shows")
    suspend fun search(
        @Query("q") query: String
    ): List<SearchResultData>

}