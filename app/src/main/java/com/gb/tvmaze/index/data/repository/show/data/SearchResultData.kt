package com.gb.tvmaze.index.data.repository.show.data

import com.google.gson.annotations.SerializedName

data class SearchResultData(

    @SerializedName("show")
    var showData: ShowData?,

    @SerializedName("person")
    var personData: PersonData?

)
