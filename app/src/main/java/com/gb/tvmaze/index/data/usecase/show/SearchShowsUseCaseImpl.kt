package com.gb.tvmaze.index.data.usecase.show

import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.repository.show.ShowRepository
import com.gb.tvmaze.index.core.usecase.show.SearchShowsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

class SearchShowsUseCaseImpl(
    private val defaultDispatcher: CoroutineDispatcher,
    private val showRepository: ShowRepository
) : SearchShowsUseCase {

    override fun invoke(query: String): Flow<List<Show>> =
        showRepository.search(query).flowOn(defaultDispatcher)

}