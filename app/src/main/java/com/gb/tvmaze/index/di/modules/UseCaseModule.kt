package com.gb.tvmaze.index.di.modules

import com.gb.tvmaze.index.core.usecase.favorite.GetFavoritesUseCase
import com.gb.tvmaze.index.core.usecase.favorite.RemoveFavoriteUseCase
import com.gb.tvmaze.index.core.usecase.favorite.SaveFavoriteUseCase
import com.gb.tvmaze.index.core.usecase.favorite.SortFavoritesUseCase
import com.gb.tvmaze.index.core.usecase.people.GetCrewCreditsUseCase
import com.gb.tvmaze.index.core.usecase.people.SearchPeopleUseCase
import com.gb.tvmaze.index.core.usecase.settings.ChangeAuthPreferenceUseCase
import com.gb.tvmaze.index.core.usecase.settings.GetAuthPreferenceUseCase
import com.gb.tvmaze.index.core.usecase.show.*
import com.gb.tvmaze.index.data.usecase.favorite.GetFavoritesUseCaseImpl
import com.gb.tvmaze.index.data.usecase.favorite.RemoveFavoriteUseCaseImpl
import com.gb.tvmaze.index.data.usecase.favorite.SaveFavoriteUseCaseImpl
import com.gb.tvmaze.index.data.usecase.favorite.SortFavoritesUseCaseImpl
import com.gb.tvmaze.index.data.usecase.people.GetCrewCreditsUseCaseImpl
import com.gb.tvmaze.index.data.usecase.people.SearchPeopleUseCaseImpl
import com.gb.tvmaze.index.data.usecase.settings.ChangeAuthPreferenceUseCaseImpl
import com.gb.tvmaze.index.data.usecase.settings.GetAuthPreferenceUseCaseImpl
import com.gb.tvmaze.index.data.usecase.show.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.dsl.module

@FlowPreview
@ExperimentalCoroutinesApi
val useCaseModule = module {
    single<ListShowsUseCase> { ListShowsUseCaseImpl(Dispatchers.IO, get()) }
    single<GetShowDetailsUseCase> { GetShowDetailsUseCaseImpl(Dispatchers.IO, get()) }
    single<GetShowImagesUseCase> { GetShowImagesUseCaseImpl(Dispatchers.IO, get()) }
    single<GetShowEpisodesUseCase> { GetShowEpisodesUseCaseImpl(Dispatchers.IO, get()) }
    single<SearchShowsUseCase> { SearchShowsUseCaseImpl(Dispatchers.IO, get()) }
    single<SearchPeopleUseCase> { SearchPeopleUseCaseImpl(Dispatchers.IO, get()) }
    single<SaveFavoriteUseCase> { SaveFavoriteUseCaseImpl(get()) }
    single<GetFavoritesUseCase> { GetFavoritesUseCaseImpl(get()) }
    single<SortFavoritesUseCase> { SortFavoritesUseCaseImpl(get()) }
    single<RemoveFavoriteUseCase> { RemoveFavoriteUseCaseImpl(get()) }
    single<GetCrewCreditsUseCase> { GetCrewCreditsUseCaseImpl(Dispatchers.IO, get()) }
    single<ChangeAuthPreferenceUseCase> { ChangeAuthPreferenceUseCaseImpl(get()) }
    single<GetAuthPreferenceUseCase> { GetAuthPreferenceUseCaseImpl(get()) }
}
