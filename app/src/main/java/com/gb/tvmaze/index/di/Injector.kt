package com.gb.tvmaze.index.di

import com.gb.tvmaze.index.di.modules.dataModule
import com.gb.tvmaze.index.di.modules.useCaseModule
import com.gb.tvmaze.index.di.modules.viewModelModule

object Injector {

    val modules = listOf(
        dataModule,
        useCaseModule,
        viewModelModule
    )

}
