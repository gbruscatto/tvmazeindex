package com.gb.tvmaze.index.presentation.view.search.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.ViewPersonDetailBinding
import com.gb.tvmaze.index.presentation.extension.toUri
import com.gb.tvmaze.index.presentation.view.search.SearchViewItem
import com.gb.tvmaze.index.presentation.view.search.adapter.CrewCreditsAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class PersonBottomSheetDialog(
    private val person: SearchViewItem.PersonItem,
    private val crewCredits: List<SearchViewItem.ShowItem>
) : BottomSheetDialogFragment() {

    private var _binding: ViewPersonDetailBinding? = null
    private val binding get() = _binding!!

    var onShowClick: ((Int) -> Unit)? = null

    private var adapter: CrewCreditsAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        ViewPersonDetailBinding
            .inflate(inflater, container, false).apply {
                _binding = this
                return this.root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initViews()
    }

    private fun initAdapter() {
        adapter = CrewCreditsAdapter(emptyList())
        adapter?.onShowClick = {
            onShowClick?.invoke(it)
        }
    }

    private fun initViews() {
        setNameGenderAndBirthday()
        setPoster()
        setCrewCredits()
    }

    private fun setNameGenderAndBirthday() {
        with(binding) {
            name.text = person.name
            person.gender?.let { gender.text = getString(R.string.gender, it) }
            person.birthday?.let { birthday.text = getString(R.string.birthday, it) }
        }
    }

    private fun setPoster() {
        with(binding) {
            person.imagePreview?.let {
                Glide.with(root)
                    .load(it.toUri())
                    .placeholder(R.drawable.layout_person_poster_placeholder)
                    .centerInside()
                    .into(poster)
            }
        }
    }

    private fun setCrewCredits() {
        with(binding) {
            if(crewCredits.isNotEmpty()) {
                adapter?.updateData(crewCredits)
                recyclerViewCrewCredits.adapter = adapter
            } else {
                crewCreditsLayout.visibility = View.GONE
            }
        }
    }

}