package com.gb.tvmaze.index.data.usecase.show

import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.repository.show.ShowRepository
import com.gb.tvmaze.index.core.usecase.show.GetShowDetailsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

class GetShowDetailsUseCaseImpl(
    private val defaultDispatcher: CoroutineDispatcher,
    private val showRepository: ShowRepository
) : GetShowDetailsUseCase {

    override fun invoke(id: Int): Flow<Show> =
        showRepository.getDetails(id).flowOn(defaultDispatcher)

}