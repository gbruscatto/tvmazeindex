package com.gb.tvmaze.index.data.repository.people.mapper

import com.gb.tvmaze.index.core.entity.Person
import com.gb.tvmaze.index.data.repository.show.data.PersonData
import java.net.URL


fun PersonData.toDomain() = Person(
    id,
    name,
    gender,
    birthday,
    imagePreview?.medium.toURL()
)

private fun String?.toURL(): URL? =
    this?.let { runCatching { URL(it) }.getOrNull() }
