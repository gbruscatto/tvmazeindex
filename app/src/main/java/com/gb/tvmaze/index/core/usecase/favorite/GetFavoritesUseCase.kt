package com.gb.tvmaze.index.core.usecase.favorite

import com.gb.tvmaze.index.core.entity.Show

interface GetFavoritesUseCase {

    fun invoke(): List<Show>

}