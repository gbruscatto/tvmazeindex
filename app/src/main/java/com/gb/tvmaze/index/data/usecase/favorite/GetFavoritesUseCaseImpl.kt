package com.gb.tvmaze.index.data.usecase.favorite

import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository
import com.gb.tvmaze.index.core.usecase.favorite.GetFavoritesUseCase

class GetFavoritesUseCaseImpl(
    private val favoriteRepository: FavoriteRepository
) : GetFavoritesUseCase {

    override fun invoke(): List<Show> =
        favoriteRepository.getFavorites()

}