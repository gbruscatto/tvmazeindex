package com.gb.tvmaze.index.core.usecase.favorite

import com.gb.tvmaze.index.core.entity.Show

interface SortFavoritesUseCase {

    fun invoke(): List<Show>

}