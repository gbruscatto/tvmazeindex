package com.gb.tvmaze.index.data.usecase.settings

import com.gb.tvmaze.index.core.repository.settings.SettingsRepository
import com.gb.tvmaze.index.core.repository.settings.SettingsRepository.Companion.AUTH_KEY
import com.gb.tvmaze.index.core.usecase.settings.ChangeAuthPreferenceUseCase

class ChangeAuthPreferenceUseCaseImpl(
    private val settingsRepository: SettingsRepository
) : ChangeAuthPreferenceUseCase {

    override fun invoke(value: Boolean) {
        settingsRepository.saveBoolean(AUTH_KEY, value)
    }

}