package com.gb.tvmaze.index.core.entity

data class SearchResult(
    var show: Show?,
    var person: Person?
)