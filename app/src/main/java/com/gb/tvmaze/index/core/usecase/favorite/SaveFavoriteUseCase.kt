package com.gb.tvmaze.index.core.usecase.favorite

import com.gb.tvmaze.index.core.entity.Show

interface SaveFavoriteUseCase {

    fun invoke(show: Show)

}