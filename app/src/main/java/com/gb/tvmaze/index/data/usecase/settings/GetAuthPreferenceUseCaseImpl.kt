package com.gb.tvmaze.index.data.usecase.settings

import com.gb.tvmaze.index.core.repository.settings.SettingsRepository
import com.gb.tvmaze.index.core.repository.settings.SettingsRepository.Companion.AUTH_KEY
import com.gb.tvmaze.index.core.usecase.settings.GetAuthPreferenceUseCase

class GetAuthPreferenceUseCaseImpl(
    private val settingsRepository: SettingsRepository
) : GetAuthPreferenceUseCase {

    override fun invoke(): Boolean =
        settingsRepository.getBoolean(AUTH_KEY)

}