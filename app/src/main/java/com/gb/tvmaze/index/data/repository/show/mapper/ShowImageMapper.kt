package com.gb.tvmaze.index.data.repository.show.mapper

import com.gb.tvmaze.index.core.entity.ShowImage
import com.gb.tvmaze.index.core.entity.ShowImageType
import com.gb.tvmaze.index.data.repository.show.data.ShowImageData
import java.net.URL


private const val POSTER = "poster"
private const val BANNER = "banner"
private const val BACKGROUND = "background"
private const val TYPOGRAPHY = "typography"

fun ShowImageData.toDomain() = ShowImage(
    type = when (type) {
        POSTER -> ShowImageType.POSTER
        BANNER -> ShowImageType.BANNER
        BACKGROUND -> ShowImageType.BACKGROUND
        TYPOGRAPHY -> ShowImageType.TYPOGRAPHY
        else -> ShowImageType.UNKNOWN
    },
    url = showImageResolutions.original.url.toURL()
)

private fun String?.toURL(): URL? =
    this?.let { runCatching { URL(it) }.getOrNull() }
