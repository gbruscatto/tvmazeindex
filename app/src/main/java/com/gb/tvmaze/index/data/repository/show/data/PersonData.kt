package com.gb.tvmaze.index.data.repository.show.data

import com.google.gson.annotations.SerializedName

data class PersonData(

    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,

    @SerializedName("birthday")
    var birthday: String?,

    @SerializedName("gender")
    var gender: String?,

    @SerializedName("image")
    var imagePreview: ShowImagePreviewData?

)
