package com.gb.tvmaze.index.data.repository.show.data

import com.google.gson.annotations.SerializedName

data class CrewCreditsEmbeddedData(

    @SerializedName("show")
    var showData: ShowData

)
