package com.gb.tvmaze.index.core.usecase.show

import com.gb.tvmaze.index.core.entity.ShowEpisode
import kotlinx.coroutines.flow.Flow

interface GetShowEpisodesUseCase {

    fun invoke(id: Int): Flow<List<ShowEpisode>>

}