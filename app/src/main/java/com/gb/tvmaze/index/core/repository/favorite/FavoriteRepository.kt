package com.gb.tvmaze.index.core.repository.favorite

import com.gb.tvmaze.index.core.entity.Show

interface FavoriteRepository {

    fun saveFavorite(show: Show)

    fun removeFavorite(id: Int)

    fun getFavorites(): List<Show>

    fun sortFavorites(): List<Show>

    companion object {
        const val FAVORITES = "favorites"
    }
}