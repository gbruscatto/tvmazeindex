package com.gb.tvmaze.index.core.entity

import java.net.URL

data class Show(
    var id: Int,
    var name: String,
    var summary: String,
    var rating: Double?,
    var imagePreview: URL?,
    var genres: List<String>,
    var status: ShowStatus,
    var schedule: ShowSchedule?
)

enum class ShowStatus {
    RUNNING,
    ENDED,
    UNKNOWN
}