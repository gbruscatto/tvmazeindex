package com.gb.tvmaze.index.core.usecase.people

import com.gb.tvmaze.index.core.entity.Person
import kotlinx.coroutines.flow.Flow

interface SearchPeopleUseCase {

    fun invoke(query: String): Flow<List<Person>>

}