package com.gb.tvmaze.index.data.usecase.show

import androidx.paging.PagingData
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.repository.show.ShowRepository
import com.gb.tvmaze.index.core.usecase.show.ListShowsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

class ListShowsUseCaseImpl(
    private val defaultDispatcher: CoroutineDispatcher,
    private val showRepository: ShowRepository
) : ListShowsUseCase {

    override fun invoke(): Flow<PagingData<Show>> {
        return showRepository.list().flowOn(defaultDispatcher)
    }

}