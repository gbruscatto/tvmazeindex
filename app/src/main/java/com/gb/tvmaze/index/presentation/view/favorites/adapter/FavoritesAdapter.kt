package com.gb.tvmaze.index.presentation.view.favorites.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.bumptech.glide.Glide
import com.gb.tvmaze.index.databinding.ComponentFavoriteItemBinding
import com.gb.tvmaze.index.presentation.extension.spannableFromHtml
import com.gb.tvmaze.index.presentation.extension.toUri
import com.gb.tvmaze.index.presentation.listener.setSafeClickListener
import com.gb.tvmaze.index.presentation.view.favorites.FavoriteViewItem

class FavoritesAdapter(
    private var items: MutableList<FavoriteViewItem>
) : Adapter<FavoritesAdapter.FavoriteViewHolder>() {

    var onShowClick: ((Int) -> Unit)? = null
    var onUnfavoriteShow: ((Int) -> Unit)? = null
    var onEmptyList: ((Boolean) -> Unit)? = null

    fun updateData(items: MutableList<FavoriteViewItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        ComponentFavoriteItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ).apply { return FavoriteViewHolder(this) }
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        holder.bind(items[position])
        setupShowClickListener(holder, items[position])
        setupUnfavoriteClick(holder, position)
    }

    private fun setupShowClickListener(holder: FavoriteViewHolder, item: FavoriteViewItem) {
        holder.binding.container.setSafeClickListener {
            onShowClick?.invoke(item.id)
        }
    }

    private fun setupUnfavoriteClick(holder: FavoriteViewHolder, position: Int) {
        holder.binding.unfavorite.setSafeClickListener {
            onUnfavoriteShow?.invoke(items[position].id)
            items.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, items.size)
            if(items.isEmpty()) onEmptyList?.invoke(true)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class FavoriteViewHolder(
        val binding: ComponentFavoriteItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: FavoriteViewItem) {
            with(binding) {
                name.text = item.name
                summary.text = item.summary?.spannableFromHtml()
                posterPlaceholder.visibility = View.VISIBLE
                item.imagePreview?.let {
                    Glide.with(root)
                        .load(it.toUri())
                        .centerInside()
                        .into(poster)
                    posterPlaceholder.visibility = View.GONE
                }
            }
        }

    }

}
