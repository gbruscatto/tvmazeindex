package com.gb.tvmaze.index.presentation.util

import android.content.SharedPreferences
import android.util.Log

/**
 * Shared preferences that logs changes to values
 */
class LoggingSharedPreferences(private val tag: String, private val preferences: SharedPreferences) {

    fun setString(key: String, value: String?) = setValue(key, value)
    fun getString(key: String): String? = preferences.getString(key, null)

    fun setBoolean(key: String, value: Boolean?) = setValue(key, value)
    fun getBoolean(key: String): Boolean = preferences.getBoolean(key, false)

    /**
     * @param key the key
     * @param value the value to save, if it's [null] the key is removed from [preferences]
     */
    private fun <T> setValue(key: String, value: T?) {
        preferences.edit().apply {
            if (value == null) {
                remove(key)
                Log.i(tag, "Removed $key")
                return@apply
            }

            when (value) {
                is Boolean -> putBoolean(key, value)
                is Int -> putInt(key, value)
                is Long -> putLong(key, value)
                is String -> putString(key, value)
                else -> throw IllegalArgumentException("The value's type is not supported yet")
            }

            Log.i(tag, "Saved $key: $value")
        }.apply()
    }

}