package com.gb.tvmaze.index.core.usecase.show

import androidx.paging.PagingData
import com.gb.tvmaze.index.core.entity.Show
import kotlinx.coroutines.flow.Flow

interface ListShowsUseCase {

    fun invoke(): Flow<PagingData<Show>>

}