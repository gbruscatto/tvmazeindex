package com.gb.tvmaze.index.presentation.extension

import android.util.TypedValue
import com.gb.tvmaze.index.presentation.util.ViewUtils.resources

private fun Float.applyDimension(unit: Int) =
    TypedValue.applyDimension(unit, this, resources.displayMetrics)

fun Float.toDp() = applyDimension(TypedValue.COMPLEX_UNIT_DIP)
