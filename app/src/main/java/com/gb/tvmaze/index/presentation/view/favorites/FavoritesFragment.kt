package com.gb.tvmaze.index.presentation.view.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.FragmentFavoritesBinding
import com.gb.tvmaze.index.presentation.base.BaseFragment
import com.gb.tvmaze.index.presentation.extension.toDp
import com.gb.tvmaze.index.presentation.listener.setSafeClickListener
import com.gb.tvmaze.index.presentation.view.favorites.adapter.FavoritesAdapter
import com.gb.tvmaze.index.presentation.view.main.MainActivity
import com.gb.tvmaze.index.presentation.view.show.episodes.ShowEpisodesDecoration
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavoritesFragment : BaseFragment() {

    companion object {
        const val TAG = "FavoritesFragment"
    }

    private val viewModel by viewModel<FavoritesViewModel>()

    private var _binding: FragmentFavoritesBinding? = null
    private val binding get() = _binding!!

    private var adapter: FavoritesAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        FragmentFavoritesBinding
            .inflate(inflater, container, false).apply {
                _binding = this
                return this.root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initViews()
        observeViewModel()
        getFavorites()
    }

    fun getFavorites() {
        viewModel.getFavorites()
    }

    private fun initAdapter() {
        adapter = FavoritesAdapter(mutableListOf())
        adapter?.onShowClick = { id -> (activity as MainActivity).goToShowDetails(id) }
        adapter?.onUnfavoriteShow = { id ->
            viewModel.unfavoriteShow(id)
            Toast.makeText(
                requireContext(),
                R.string.removed_from_favorites,
                Toast.LENGTH_LONG
            ).show()
        }
        adapter?.onEmptyList = { binding.emptyPlaceholder.containerEmpty.visibility = View.VISIBLE }
    }

    override fun initViews() {
        setupAdapter()
        setupSortAlphabeticallyButton()
    }

    private fun setupSortAlphabeticallyButton() {
        binding.sortAlphabetically.setSafeClickListener {
            viewModel.sortFavoritesAlphabetically()
            Toast.makeText(
                requireContext(),
                R.string.list_sorted_alphabetically,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun setupAdapter() {
        with(binding) {
            recyclerViewFavorites.adapter = adapter
            val marginBetweenItems = 24f.toDp().toInt()
            binding.recyclerViewFavorites.addItemDecoration(
                ShowEpisodesDecoration(marginBetweenItems)
            )
        }
    }

    override fun observeViewModel() {
        viewModel.favorites.observe(viewLifecycleOwner, {
            if(it.isNotEmpty()) {
                binding.emptyPlaceholder.containerEmpty.visibility = View.GONE
                binding.sortAlphabetically.visibility = View.VISIBLE
                adapter?.updateData(it.toMutableList())
            } else {
                binding.sortAlphabetically.visibility = View.GONE
                binding.emptyPlaceholder.containerEmpty.visibility = View.VISIBLE
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter = null
        _binding = null
    }
}