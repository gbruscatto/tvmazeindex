package com.gb.tvmaze.index.core.entity

import java.net.URL

data class Person(
    var id: Int,
    var name: String,
    var gender: String?,
    var birthday: String?,
    var imagePreview: URL?
)