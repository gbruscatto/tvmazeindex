package com.gb.tvmaze.index.core.repository.people

import com.gb.tvmaze.index.core.entity.Person
import com.gb.tvmaze.index.core.entity.Show
import kotlinx.coroutines.flow.Flow

interface PeopleRepository {

    fun search(query: String): Flow<List<Person>>

    fun getCrewCredits(id: Int): Flow<List<Show>>

}