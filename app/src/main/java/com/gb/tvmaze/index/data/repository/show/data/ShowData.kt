package com.gb.tvmaze.index.data.repository.show.data

import com.google.gson.annotations.SerializedName

data class ShowData(

    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,

    @SerializedName("summary")
    var summary: String?,

    @SerializedName("rating")
    var rating: ShowRatingData?,

    @SerializedName("image")
    var imagePreview: ShowImagePreviewData?,

    @SerializedName("genres")
    var genres: List<String>?,

    @SerializedName("status")
    var status: String?,

    @SerializedName("schedule")
    var showScheduleData: ShowScheduleData?

)
