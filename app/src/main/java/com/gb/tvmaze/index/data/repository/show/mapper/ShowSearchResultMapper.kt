package com.gb.tvmaze.index.data.repository.show.mapper

import com.gb.tvmaze.index.core.entity.SearchResult
import com.gb.tvmaze.index.data.repository.people.mapper.toDomain
import com.gb.tvmaze.index.data.repository.show.data.SearchResultData


fun SearchResultData.toDomain() = SearchResult(
    showData?.toDomain(),
    personData?.toDomain()
)
