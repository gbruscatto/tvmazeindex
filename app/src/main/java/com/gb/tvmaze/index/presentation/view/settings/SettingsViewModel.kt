package com.gb.tvmaze.index.presentation.view.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.usecase.settings.ChangeAuthPreferenceUseCase
import com.gb.tvmaze.index.core.usecase.settings.GetAuthPreferenceUseCase
import com.gb.tvmaze.index.core.usecase.show.ListShowsUseCase
import kotlinx.coroutines.flow.Flow

class SettingsViewModel(
    private val changeAuthPreferenceUseCase: ChangeAuthPreferenceUseCase,
    private val getAuthPreferenceUseCase: GetAuthPreferenceUseCase
) : ViewModel() {

    fun changeAuthPreference(value: Boolean) =
        changeAuthPreferenceUseCase.invoke(value)

    fun isAuthModeActive(): Boolean =
        getAuthPreferenceUseCase.invoke()

}