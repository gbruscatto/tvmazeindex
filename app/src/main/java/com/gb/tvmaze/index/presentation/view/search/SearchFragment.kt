package com.gb.tvmaze.index.presentation.view.search

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.FragmentSearchBinding
import com.gb.tvmaze.index.presentation.alert.ProgressState
import com.gb.tvmaze.index.presentation.base.BaseFragment
import com.gb.tvmaze.index.presentation.listener.setSafeClickListener
import com.gb.tvmaze.index.presentation.view.search.result.PeopleSearchResultFragment
import com.gb.tvmaze.index.presentation.view.search.result.ShowSearchResultFragment
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class SearchFragment : BaseFragment() {

    companion object {
        const val TAG = "SearchFragment"
    }

    private val viewModel by sharedViewModel<SearchViewModel>()

    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    private var stateAdapter: SearchStateAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        FragmentSearchBinding
            .inflate(inflater, container, false).apply {
                _binding = this
                return this.root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        stateAdapter = SearchStateAdapter(this)
        initViews()
        observeViewModel()
    }

    override fun initViews() {
        setupTabLayout()
        setupSearchTextListener()
        setupResetSearchButton()
    }

    override fun observeViewModel() {
        viewModel.shows.observe(viewLifecycleOwner, {
            binding.errorLayout.container.visibility = View.GONE
        })

        viewModel.people.observe(viewLifecycleOwner, {
            binding.errorLayout.container.visibility = View.GONE
        })

        viewModel.loading.observe(viewLifecycleOwner, {
            binding.searchLoading.visibility =
                if(it is ProgressState.Loading) { View.VISIBLE }
                else { View.GONE }
        })

        viewModel.error.observe(viewLifecycleOwner, {
            binding.errorLayout.container.visibility = View.VISIBLE
        })
    }

    private fun setupTabLayout() {
        with(binding) {
            searchViewPager.adapter = stateAdapter
            TabLayoutMediator(searchTabLayout, searchViewPager) { tab, position ->
                if(position == 0) tab.text = resources.getString(R.string.shows)
                else tab.text = resources.getString(R.string.people)
            }.attach()
        }
    }

    private fun setupResetSearchButton() {
        binding.resetSearch.setSafeClickListener {
            binding.searchEditText.setText("")
        }
    }

    private fun setupSearchTextListener() {
        with(binding) {
            searchEditText.addTextChangedListener(object: TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

                override fun afterTextChanged(p0: Editable?) { }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    viewModel.query = searchEditText.text.toString()
                    hideOrShowResetSearchButton()
                    searchShowOrPeople()
                }
            })
        }
    }

    private fun searchShowOrPeople() {
        with(binding) {
            if(searchViewPager.currentItem == 0) {
                stateAdapter?.showSearchResultFragment
                    ?.search(searchEditText.text.toString())
            } else {
                stateAdapter?.peopleSearchResultFragment
                    ?.search(searchEditText.text.toString())
            }
        }
    }

    private fun hideOrShowResetSearchButton() {
        with(binding) {
            if(viewModel.query.isEmpty()) {
                resetSearch.visibility = View.INVISIBLE
                searchPlaceholder.visibility = View.VISIBLE
            } else {
                resetSearch.visibility = View.VISIBLE
                searchPlaceholder.visibility = View.INVISIBLE
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        stateAdapter = null
        _binding = null
    }

}

class SearchStateAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    val showSearchResultFragment = ShowSearchResultFragment()
    val peopleSearchResultFragment = PeopleSearchResultFragment()

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> showSearchResultFragment
            else -> peopleSearchResultFragment
        }
    }
}