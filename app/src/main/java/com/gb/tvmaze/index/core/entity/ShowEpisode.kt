package com.gb.tvmaze.index.core.entity

import java.net.URL

data class ShowEpisode(
    var id: Int,
    var name: String,
    var summary: String,
    var season: Int,
    var number: Int,
    var imagePreview: URL?,
    var rating: Double?
)