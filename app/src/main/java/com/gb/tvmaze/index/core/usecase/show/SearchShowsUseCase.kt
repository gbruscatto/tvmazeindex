package com.gb.tvmaze.index.core.usecase.show

import com.gb.tvmaze.index.core.entity.Show
import kotlinx.coroutines.flow.Flow

interface SearchShowsUseCase {

    fun invoke(query: String): Flow<List<Show>>

}