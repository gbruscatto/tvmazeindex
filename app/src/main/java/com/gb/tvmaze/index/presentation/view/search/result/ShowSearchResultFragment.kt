package com.gb.tvmaze.index.presentation.view.search.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gb.tvmaze.index.databinding.FragmentSearchResultShowBinding
import com.gb.tvmaze.index.presentation.base.BaseFragment
import com.gb.tvmaze.index.presentation.extension.toDp
import com.gb.tvmaze.index.presentation.view.main.MainActivity
import com.gb.tvmaze.index.presentation.view.search.SearchViewModel
import com.gb.tvmaze.index.presentation.view.search.adapter.ShowSearchResultAdapter
import com.gb.tvmaze.index.presentation.view.show.episodes.ShowEpisodesDecoration
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ShowSearchResultFragment : BaseFragment() {

    private val viewModel by sharedViewModel<SearchViewModel>()

    private var _binding: FragmentSearchResultShowBinding? = null
    private val binding get() = _binding!!

    private var adapter: ShowSearchResultAdapter? = null
    private var lastQuery = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        FragmentSearchResultShowBinding
            .inflate(inflater, container, false).apply {
                _binding = this
                return this.root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initViews()
        observeViewModel()
    }

    private fun initAdapter() {
        adapter = ShowSearchResultAdapter(emptyList())
        adapter?.onShowClick = { id -> (activity as MainActivity).goToShowDetails(id) }
    }

    override fun observeViewModel() {
        viewModel.shows.observe(viewLifecycleOwner, {
            if(it.isNotEmpty()) {
                binding.emptyPlaceholder.containerEmpty.visibility = View.GONE
                adapter?.updateData(it)
            } else {
                binding.emptyPlaceholder.containerEmpty.visibility = View.VISIBLE
            }
        })
    }

    override fun onResume() {
        super.onResume()

        viewModel.query.let {
            if(lastQuery != it) search(it)
        }
    }

    override fun initViews() {
        with(binding) {
            recyclerViewSearch.adapter = adapter
            val marginBetweenItems = 24f.toDp().toInt()
            binding.recyclerViewSearch.addItemDecoration(
                ShowEpisodesDecoration(marginBetweenItems)
            )
        }
    }

    fun search(query: String) {
        binding.emptyPlaceholder.containerEmpty.visibility = View.GONE
        lastQuery = query
        viewModel.searchShows(query)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        adapter = null
        _binding = null
    }
}