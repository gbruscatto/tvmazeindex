package com.gb.tvmaze.index.data.usecase.show

import com.gb.tvmaze.index.core.entity.ShowImage
import com.gb.tvmaze.index.core.repository.show.ShowRepository
import com.gb.tvmaze.index.core.usecase.show.GetShowImagesUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

class GetShowImagesUseCaseImpl(
    private val defaultDispatcher: CoroutineDispatcher,
    private val showRepository: ShowRepository
) : GetShowImagesUseCase {

    override fun invoke(id: Int): Flow<List<ShowImage>> =
        showRepository.getImages(id).flowOn(defaultDispatcher)

}