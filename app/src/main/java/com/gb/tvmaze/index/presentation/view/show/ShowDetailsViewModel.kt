package com.gb.tvmaze.index.presentation.view.show

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.entity.ShowEpisode
import com.gb.tvmaze.index.core.entity.ShowImage
import com.gb.tvmaze.index.core.usecase.favorite.GetFavoritesUseCase
import com.gb.tvmaze.index.core.usecase.favorite.RemoveFavoriteUseCase
import com.gb.tvmaze.index.core.usecase.favorite.SaveFavoriteUseCase
import com.gb.tvmaze.index.core.usecase.show.GetShowDetailsUseCase
import com.gb.tvmaze.index.core.usecase.show.GetShowEpisodesUseCase
import com.gb.tvmaze.index.core.usecase.show.GetShowImagesUseCase
import com.gb.tvmaze.index.presentation.alert.ProgressState
import com.gb.tvmaze.index.presentation.view.show.EpisodesViewItem.Episode
import com.gb.tvmaze.index.presentation.view.show.EpisodesViewItem.Season
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import java.net.URL

class ShowDetailsViewModel(
    private val detailsUseCase: GetShowDetailsUseCase,
    private val imagesUseCase: GetShowImagesUseCase,
    private val episodesUseCase: GetShowEpisodesUseCase,
    private val saveFavoriteUseCase: SaveFavoriteUseCase,
    private val removeFavoriteUseCase: RemoveFavoriteUseCase,
    private val getFavoritesUseCase: GetFavoritesUseCase
) : ViewModel() {

    var favorited = false

    val show = MutableLiveData<Show>()
    val images = MutableLiveData<List<ShowImage>>()
    val episodes = MutableLiveData<List<EpisodesViewItem>>()
    val loading: MutableLiveData<ProgressState> = MutableLiveData(ProgressState.Nothing)
    val error = MutableLiveData<ShowDetailsError>()

    fun getDetails(id: Int) {
        viewModelScope.launch {
            detailsUseCase.invoke(id).onStart {
                loading.value = ProgressState.Loading
            }.catch {
                loading.value = ProgressState.Nothing
                error.value = ShowDetailsError.DETAILS_ERROR
            }.collect {
                show.value = it
            }
        }
    }

    fun getImages(id: Int) {
        viewModelScope.launch {
            imagesUseCase.invoke(id).collect {
                images.value = it
            }
        }
    }

    fun getEpisodes(id: Int) {
        viewModelScope.launch {
            episodesUseCase.invoke(id).map {
                episodesToViewItem(it)
            }.onStart {
                loading.value = ProgressState.Loading
            }.catch {
                loading.value = ProgressState.Nothing
                error.value = ShowDetailsError.EPISODES_ERROR
            }.collect {
                loading.value = ProgressState.Nothing
                episodes.value = it
            }
        }
    }

    fun favoriteShow() {
        show.value?.let { saveFavoriteUseCase.invoke(it) }
    }

    fun unfavoriteShow() {
        show.value?.let { removeFavoriteUseCase.invoke(it.id) }
    }

    fun isFavorite(): Boolean {
        return getFavoritesUseCase.invoke().contains(show.value)
    }

    private fun episodesToViewItem(
        showEpisodes: List<ShowEpisode>
    ): List<EpisodesViewItem> {
        if(showEpisodes.isEmpty()) return emptyList()

        val recyclerViewList = mutableListOf<EpisodesViewItem>()
        var currentSeason = showEpisodes[0].season
        recyclerViewList.add(Season(currentSeason))

        showEpisodes.forEach {
            if(it.season != currentSeason) {
                currentSeason = it.season
                recyclerViewList.add(Season(it.season))
            }

            recyclerViewList.add(
                Episode(
                    it.id,
                    it.name,
                    it.summary,
                    it.season,
                    it.number,
                    it.rating,
                    it.imagePreview
                )
            )
        }

        return recyclerViewList
    }

}

sealed class EpisodesViewItem {
    class Season(
        val number: Int
    ): EpisodesViewItem()

    class Episode(
        val id: Int,
        var name: String,
        var summary: String,
        var season: Int,
        var number: Int,
        var rating: Double?,
        val imagePreview: URL?,
    ): EpisodesViewItem()
}

enum class ShowDetailsError {
    DETAILS_ERROR, EPISODES_ERROR
}