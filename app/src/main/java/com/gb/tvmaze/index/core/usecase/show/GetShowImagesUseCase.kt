package com.gb.tvmaze.index.core.usecase.show

import com.gb.tvmaze.index.core.entity.ShowImage
import kotlinx.coroutines.flow.Flow

interface GetShowImagesUseCase {

    fun invoke(id: Int): Flow<List<ShowImage>>

}