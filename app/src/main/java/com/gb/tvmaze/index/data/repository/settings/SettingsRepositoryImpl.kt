package com.gb.tvmaze.index.data.repository.settings

import android.content.Context
import com.gb.tvmaze.index.core.repository.settings.SettingsRepository
import com.gb.tvmaze.index.core.repository.settings.SettingsRepository.Companion.SETTINGS
import com.gb.tvmaze.index.presentation.util.LoggingSharedPreferences

class SettingsRepositoryImpl(context: Context) : SettingsRepository {

    companion object {
        const val TAG = "SettingRepositoryImpl"
    }

    var settingsShared =
        LoggingSharedPreferences(
            TAG, context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
        )

    override fun saveBoolean(key: String, value: Boolean) {
        settingsShared.setBoolean(key, value)
    }

    override fun getBoolean(key: String): Boolean =
        settingsShared.getBoolean(key)

}