package com.gb.tvmaze.index.presentation.view.show.episodes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.bumptech.glide.Glide
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.ComponentEpisodeItemBinding
import com.gb.tvmaze.index.databinding.ComponentSeasonItemBinding
import com.gb.tvmaze.index.presentation.extension.spannableFromHtml
import com.gb.tvmaze.index.presentation.extension.toStringTwoDigits
import com.gb.tvmaze.index.presentation.extension.toUri
import com.gb.tvmaze.index.presentation.view.show.EpisodesViewItem

class ShowEpisodesAdapter(
    private val items: List<EpisodesViewItem>
) : Adapter<RecyclerView.ViewHolder>() {

    var onEpisodeClick: ((EpisodesViewItem.Episode) -> Unit)? = null

    override fun getItemViewType(position: Int): Int {
        return when(items[position]) {
            is EpisodesViewItem.Episode -> R.layout.component_episode_item
            is EpisodesViewItem.Season -> R.layout.component_season_item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            R.layout.component_episode_item -> ShowEpisodeViewHolder(
                ComponentEpisodeItemBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            R.layout.component_season_item -> ShowSeasonViewHolder(
                ComponentSeasonItemBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )

            else -> throw IllegalArgumentException("Invalid view type provided.")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is ShowEpisodeViewHolder -> {
                val episode = items[position] as EpisodesViewItem.Episode
                holder.bind(episode)
                setupEpisodeClickListener(holder, episode)
            }
            is ShowSeasonViewHolder -> {
                val season = items[position] as EpisodesViewItem.Season
                holder.bind(season)
            }
        }
    }

    private fun setupEpisodeClickListener(
        holder: ShowEpisodeViewHolder,
        episode: EpisodesViewItem.Episode
    ) {
        holder.binding.root.setOnClickListener {
            onEpisodeClick?.invoke(episode)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ShowSeasonViewHolder(
        private val binding: ComponentSeasonItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: EpisodesViewItem.Season) {
            with(binding) {
                """Season ${item.number}""".also { name.text = it }
            }
        }
    }

    class ShowEpisodeViewHolder(
        val binding: ComponentEpisodeItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: EpisodesViewItem.Episode) {
            with(binding) {
                name.text = item.name
                summary.text = item.summary.spannableFromHtml()
                number.text = item.number.toStringTwoDigits()
                item.imagePreview?.let {
                    Glide.with(root)
                        .load(it.toUri())
                        .placeholder(R.drawable.layout_show_episode_placeholder)
                        .centerInside()
                        .into(image)
                }
            }
        }
    }

}
