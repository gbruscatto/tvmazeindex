package com.gb.tvmaze.index.presentation.base

import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    protected open fun initViews() {}
    protected open fun observeViewModel() {}
}