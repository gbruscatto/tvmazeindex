package com.gb.tvmaze.index.core.usecase.people

import com.gb.tvmaze.index.core.entity.Show
import kotlinx.coroutines.flow.Flow

interface GetCrewCreditsUseCase {

    fun invoke(id: Int): Flow<List<Show>>

}