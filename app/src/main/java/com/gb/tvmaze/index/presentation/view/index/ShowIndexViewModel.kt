package com.gb.tvmaze.index.presentation.view.index

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.usecase.show.ListShowsUseCase
import kotlinx.coroutines.flow.Flow

class ShowIndexViewModel(
    private val listShowsUseCase: ListShowsUseCase
) : ViewModel() {

    fun listShows(): Flow<PagingData<Show>> =
        listShowsUseCase.invoke().cachedIn(viewModelScope)

}