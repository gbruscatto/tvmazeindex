package com.gb.tvmaze.index.data.usecase.people

import com.gb.tvmaze.index.core.entity.Person
import com.gb.tvmaze.index.core.repository.people.PeopleRepository
import com.gb.tvmaze.index.core.usecase.people.SearchPeopleUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

class SearchPeopleUseCaseImpl(
    private val defaultDispatcher: CoroutineDispatcher,
    private val peopleRepository: PeopleRepository
) : SearchPeopleUseCase {

    override fun invoke(query: String): Flow<List<Person>> =
        peopleRepository.search(query).flowOn(defaultDispatcher)

}