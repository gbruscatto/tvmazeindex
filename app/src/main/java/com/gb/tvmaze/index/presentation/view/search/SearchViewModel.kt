package com.gb.tvmaze.index.presentation.view.search


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gb.tvmaze.index.core.entity.Person
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.usecase.people.GetCrewCreditsUseCase
import com.gb.tvmaze.index.core.usecase.people.SearchPeopleUseCase
import com.gb.tvmaze.index.core.usecase.show.SearchShowsUseCase
import com.gb.tvmaze.index.presentation.alert.ProgressState
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import java.net.URL

class SearchViewModel(
    private val searchShowsUseCase: SearchShowsUseCase,
    private val searchPeopleUseCase: SearchPeopleUseCase,
    private val getCrewCreditsUseCase: GetCrewCreditsUseCase
) : ViewModel() {

    var query = ""
    val shows = MutableLiveData<List<SearchViewItem.ShowItem>>()
    val people = MutableLiveData<List<SearchViewItem.PersonItem>>()
    val crewCredits = MutableLiveData<List<SearchViewItem.ShowItem>>()
    val loading: MutableLiveData<ProgressState> = MutableLiveData(ProgressState.Nothing)
    val error = MutableLiveData<SearchError>()

    var currentPerson: SearchViewItem.PersonItem? = null

    fun searchShows(query: String) {
        viewModelScope.launch {
            searchShowsUseCase.invoke(query).map {
                showResultsToViewItem(it)
            }.onStart {
                loading.value = ProgressState.Loading
            }.catch {
                loading.value = ProgressState.Nothing
                error.value = SearchError.UNKNOWN
            }.collect {
                loading.value = ProgressState.Nothing
                shows.value = it
            }
        }
    }

    fun searchPeople(query: String) {
        viewModelScope.launch {
            searchPeopleUseCase.invoke(query).map {
                peopleResultsToViewItem(it)
            }.onStart {
                loading.value = ProgressState.Loading
            }.catch {
                loading.value = ProgressState.Nothing
                error.value = SearchError.UNKNOWN
            }.collect {
                loading.value = ProgressState.Nothing
                people.value = it
            }
        }
    }

    fun getCrewCredits(personId: Int) {
        viewModelScope.launch {
            getCrewCreditsUseCase.invoke(personId).map {
                showResultsToViewItem(it)
            }.onStart {
                loading.value = ProgressState.Loading
            }.catch {
                loading.value = ProgressState.Nothing
                error.value = SearchError.UNKNOWN
            }.collect {
                loading.value = ProgressState.Nothing
                crewCredits.value = it
            }
        }
    }

    private fun showResultsToViewItem(
        results: List<Show>
    ): List<SearchViewItem.ShowItem> {
        val viewItemList = mutableListOf<SearchViewItem.ShowItem>()
        results.forEach { viewItemList.add(SearchViewItem.showToViewItem(it)) }
        return viewItemList
    }

    private fun peopleResultsToViewItem(
        results: List<Person>
    ): List<SearchViewItem.PersonItem> {
        val viewItemList = mutableListOf<SearchViewItem.PersonItem>()
        results.forEach { viewItemList.add(SearchViewItem.personToViewItem(it)) }
        return viewItemList
    }

}

sealed class SearchViewItem {
    class ShowItem(
        val id: Int,
        var name: String,
        var summary: String?,
        val imagePreview: URL?,
    ): SearchViewItem()

    class PersonItem(
        val id: Int,
        var name: String,
        var gender: String?,
        var birthday: String?,
        val imagePreview: URL?
    ): SearchViewItem()

    companion object {
        fun showToViewItem(show: Show): ShowItem =
            ShowItem(
                show.id,
                show.name,
                show.summary,
                show.imagePreview
            )

        fun personToViewItem(person: Person): PersonItem =
            PersonItem (
                person.id,
                person.name,
                person.gender,
                person.birthday,
                person.imagePreview
            )
    }
}

enum class SearchError {
    UNKNOWN
}
