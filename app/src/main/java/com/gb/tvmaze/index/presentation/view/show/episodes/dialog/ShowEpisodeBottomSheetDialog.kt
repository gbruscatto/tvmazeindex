package com.gb.tvmaze.index.presentation.view.show.episodes.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.ViewEpisodeDetailBinding
import com.gb.tvmaze.index.presentation.extension.spannableFromHtml
import com.gb.tvmaze.index.presentation.extension.toStringTwoDigits
import com.gb.tvmaze.index.presentation.extension.toUri
import com.gb.tvmaze.index.presentation.view.show.EpisodesViewItem.Episode
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ShowEpisodeBottomSheetDialog(
    private val episode: Episode
) : BottomSheetDialogFragment() {

    private var _binding: ViewEpisodeDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        ViewEpisodeDetailBinding
            .inflate(inflater, container, false).apply {
                _binding = this
                return this.root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        with(binding) {
            name.text = episode.name

            summary.text = episode.summary.spannableFromHtml()

            seasonAndNumber.text = getString(
                R.string.season_and_number,
                episode.season.toStringTwoDigits(),
                episode.number.toStringTwoDigits()
            )

            episode.rating?.let {
                rating.text = it.toString()
            }?: run {
                ratingStar.visibility = View.GONE
                rating.visibility = View.GONE
            }

            episode.imagePreview?.let {
                Glide.with(root)
                    .load(it.toUri())
                    .placeholder(R.drawable.layout_show_episode_placeholder)
                    .centerInside()
                    .into(image)
            }
        }
    }
}