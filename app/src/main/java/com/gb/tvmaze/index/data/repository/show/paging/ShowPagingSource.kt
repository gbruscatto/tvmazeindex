package com.gb.tvmaze.index.data.repository.show.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.gb.tvmaze.index.data.repository.show.api.ShowApiService
import com.gb.tvmaze.index.data.repository.show.data.ShowData
import retrofit2.HttpException
import java.io.IOException

private const val DEFAULT_PAGE_INDEX = 0

class ShowPagingSource(
    private val apiService: ShowApiService
) : PagingSource<Int, ShowData>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ShowData> {
        val page = params.key ?: DEFAULT_PAGE_INDEX
        return try {
            val response = apiService.list(page)
            LoadResult.Page(
                data = response,
                prevKey = if (page == DEFAULT_PAGE_INDEX) null else page - 1,
                nextKey = if (response.isEmpty()) null else page + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, ShowData>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}