package com.gb.tvmaze.index

import android.app.Application
import com.gb.tvmaze.index.di.Injector
import com.gb.tvmaze.index.presentation.util.ViewUtils
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class Application: Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
        ViewUtils.init(this)
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@Application)
            modules(Injector.modules)
        }
    }

}