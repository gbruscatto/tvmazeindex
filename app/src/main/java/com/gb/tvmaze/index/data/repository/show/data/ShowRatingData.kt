package com.gb.tvmaze.index.data.repository.show.data

import com.google.gson.annotations.SerializedName

data class ShowRatingData(

    @SerializedName("average")
    var average: Double?

)
