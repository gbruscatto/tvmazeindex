package com.gb.tvmaze.index.presentation.view.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.bumptech.glide.Glide
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.ComponentSearchPersonItemBinding
import com.gb.tvmaze.index.presentation.extension.toUri
import com.gb.tvmaze.index.presentation.view.search.SearchViewItem

class PeopleSearchResultAdapter(
    private var items: List<SearchViewItem.PersonItem>
) : Adapter<PeopleSearchResultAdapter.PersonViewHolder>() {

    var onPersonClick: ((SearchViewItem.PersonItem) -> Unit)? = null

    fun updateData(items: List<SearchViewItem.PersonItem>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        ComponentSearchPersonItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ).apply { return PersonViewHolder(this) }
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        holder.bind(items[position])
        setupPersonClickListener(holder, items[position])
    }

    private fun setupPersonClickListener(holder: PersonViewHolder, person: SearchViewItem.PersonItem) {
        holder.binding.container.setOnClickListener {
            onPersonClick?.invoke(person)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class PersonViewHolder(
        val binding: ComponentSearchPersonItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SearchViewItem.PersonItem) {
            with(binding) {
                name.text = item.name
                item.gender?.let { gender.text = itemView.context.getString(R.string.gender, it) }
                item.birthday?.let { birthday.text = itemView.context.getString(R.string.birthday, it) }
                item.imagePreview?.let {
                    Glide.with(root)
                        .load(it.toUri())
                        .placeholder(R.drawable.layout_search_placeholder)
                        .centerInside()
                        .into(poster)
                }
            }
        }

    }

}
