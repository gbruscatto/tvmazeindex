package com.gb.tvmaze.index.core.repository.show

import androidx.paging.PagingData
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.entity.ShowEpisode
import com.gb.tvmaze.index.core.entity.ShowImage
import kotlinx.coroutines.flow.Flow

interface ShowRepository {

    fun list(): Flow<PagingData<Show>>

    fun getDetails(id: Int): Flow<Show>

    fun getImages(id: Int): Flow<List<ShowImage>>

    fun getEpisodes(id: Int): Flow<List<ShowEpisode>>

    fun search(query: String): Flow<List<Show>>

}