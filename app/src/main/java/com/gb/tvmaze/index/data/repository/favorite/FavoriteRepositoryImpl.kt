package com.gb.tvmaze.index.data.repository.favorite

import android.content.Context
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository
import com.gb.tvmaze.index.core.repository.favorite.FavoriteRepository.Companion.FAVORITES
import com.gb.tvmaze.index.presentation.util.LoggingSharedPreferences
import com.google.gson.Gson

class FavoriteRepositoryImpl(context: Context) : FavoriteRepository {

    companion object {
        const val TAG = "FavoriteRepositoryImpl"
    }

    private val gson = Gson()

    var favoritesShared =
        LoggingSharedPreferences(
            TAG, context.getSharedPreferences(FAVORITES, Context.MODE_PRIVATE)
        )

    override fun saveFavorite(show: Show) {
        var list = mutableListOf<Show>()
        favoritesShared.getString(FAVORITES)?.let {
            list = gson.fromJson(it, Array<Show>::class.java).toMutableList()
        }

        if(!list.contains(show)) {
            list.add(show)
            favoritesShared.setString(FAVORITES, gson.toJson(list))
        }
    }

    override fun removeFavorite(id: Int) {
        var list = mutableListOf<Show>()
        favoritesShared.getString(FAVORITES)?.let {
            list = gson.fromJson(it, Array<Show>::class.java).toMutableList()
        }

        list.find { it.id == id }?.let {
            list.remove(it)
            favoritesShared.setString(FAVORITES, gson.toJson(list))
        }
    }

    override fun getFavorites(): List<Show> =
        favoritesShared.getString(FAVORITES)?.let {
            gson.fromJson(it, Array<Show>::class.java).toMutableList()
        }?: run { emptyList() }

    override fun sortFavorites(): List<Show> =
        favoritesShared.getString(FAVORITES)?.let {
            val list = gson.fromJson(it, Array<Show>::class.java).toMutableList()
            list.sortBy { show -> show.name }
            favoritesShared.setString(FAVORITES, gson.toJson(list))
            list
        }?: run {
            emptyList()
        }

}