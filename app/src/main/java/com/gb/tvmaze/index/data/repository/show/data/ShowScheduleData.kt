package com.gb.tvmaze.index.data.repository.show.data

import com.google.gson.annotations.SerializedName

data class ShowScheduleData(

    @SerializedName("time")
    var time: String,

    @SerializedName("days")
    var days: List<String>?,

)
