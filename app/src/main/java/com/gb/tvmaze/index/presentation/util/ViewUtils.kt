package com.gb.tvmaze.index.presentation.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources

@SuppressLint("StaticFieldLeak")
object ViewUtils {
    var context: Context? = null

    fun init(context: Context?) {
        ViewUtils.context = context
    }

    val resources: Resources
        get() = context!!.resources

}
