package com.gb.tvmaze.index.presentation.view.index

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.core.entity.Show
import com.gb.tvmaze.index.databinding.ComponentShowItemBinding
import com.gb.tvmaze.index.presentation.extension.toUri
import com.gb.tvmaze.index.presentation.listener.setSafeClickListener
import com.gb.tvmaze.index.presentation.view.index.ShowIndexAdapter.ListingViewHolder

class ShowIndexAdapter: PagingDataAdapter<Show, ListingViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Show>() {
            override fun areItemsTheSame(oldItem: Show, newItem: Show): Boolean =
                oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Show, newItem: Show): Boolean =
                oldItem.id == newItem.id
        }
    }

    var onShowClick: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListingViewHolder {
        ComponentShowItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ).apply { return ListingViewHolder(this) }
    }

    override fun onBindViewHolder(holder: ListingViewHolder, position: Int) {
        holder.bind(getItem(position))
        setupShowClickListener(holder, getItem(position))
    }

    private fun setupShowClickListener(holder: ListingViewHolder, show: Show?) {
        holder.binding.poster.setSafeClickListener {
            show?.let { onShowClick?.invoke(it.id) }
        }
    }

    class ListingViewHolder(
        val binding: ComponentShowItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Show?) {
            with(binding) {
                name.text = item?.name
                item?.imagePreview?.let {
                    Glide.with(root)
                        .load(it.toUri())
                        .centerInside()
                        .placeholder(R.drawable.layout_show_index_placeholder)
                        .into(poster)
                }
            }
        }
    }
}