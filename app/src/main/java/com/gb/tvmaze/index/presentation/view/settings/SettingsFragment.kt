package com.gb.tvmaze.index.presentation.view.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gb.tvmaze.index.databinding.FragmentSettingsBinding
import com.gb.tvmaze.index.presentation.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class SettingsFragment : BaseFragment() {

    companion object {
        const val TAG = "SettingsFragment"
    }

    private val viewModel by viewModel<SettingsViewModel>()

    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        FragmentSettingsBinding
            .inflate(inflater, container, false).apply {
                _binding = this
                return this.root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setupAuthSwitchListener()
    }

    private fun setupAuthSwitchListener() {
        binding.securitySwitch.setOnCheckedChangeListener { _, isChecked ->
            viewModel.changeAuthPreference(isChecked)
        }
    }

    override fun initViews() {
        binding.securitySwitch.isChecked = viewModel.isAuthModeActive()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}