package com.gb.tvmaze.index.data.repository.show.data

import com.google.gson.annotations.SerializedName

data class ShowEpisodeData(

    @SerializedName("id")
    var id: Int,

    @SerializedName("name")
    var name: String,

    @SerializedName("summary")
    var summary: String?,

    @SerializedName("season")
    var season: Int,

    @SerializedName("number")
    var number: Int,

    @SerializedName("image")
    var imagePreview: ShowImagePreviewData?,

    @SerializedName("rating")
    var rating: ShowRatingData?

)
