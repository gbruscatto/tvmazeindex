package com.gb.tvmaze.index.presentation.view.index

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.gb.tvmaze.index.databinding.FragmentShowIndexBinding
import com.gb.tvmaze.index.presentation.base.BaseFragment
import com.gb.tvmaze.index.presentation.listener.setSafeClickListener
import com.gb.tvmaze.index.presentation.view.main.MainActivity
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ShowIndexFragment : BaseFragment() {

    companion object {
        const val TAG = "ShowIndexFragment"
        private const val LIST_SPAN_COUNT = 2
    }

    private val viewModel by viewModel<ShowIndexViewModel>()

    private var _binding: FragmentShowIndexBinding? = null
    private val binding get() = _binding!!

    private var showIndexAdapter: ShowIndexAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        FragmentShowIndexBinding
            .inflate(inflater, container, false).apply {
                _binding = this
                return this.root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showIndexAdapter = ShowIndexAdapter()
        initViews()
        listShows()
    }


    override fun initViews() {
        setupTryAgainButton()
        setupShowIndexAdapter()
        setupLoadStateListener()
    }

    private fun setupTryAgainButton() {
        binding.tryAgain.setSafeClickListener {
            showIndexAdapter?.retry()
        }
    }

    private fun setupLoadStateListener() {
        with(binding) {
            lifecycleScope.launch {
                showIndexAdapter?.loadStateFlow?.collectLatest { loadStates ->
                    when (loadStates.refresh) {
                        is LoadState.NotLoading -> {
                            loading.visibility = View.GONE
                            errorLayout.visibility = View.GONE

                        }
                        LoadState.Loading -> {
                            loading.visibility = View.GONE
                            loading.visibility = View.VISIBLE
                        }
                        is LoadState.Error -> {
                            loading.visibility = View.GONE
                            errorLayout.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }
    }

    private fun setupShowIndexAdapter() {
        with(binding) {
            recyclerViewShows.layoutManager =
                GridLayoutManager(context, LIST_SPAN_COUNT)
            showIndexAdapter?.onShowClick = { id -> (activity as MainActivity).goToShowDetails(id) }
            recyclerViewShows.adapter = showIndexAdapter
        }
    }

    private fun listShows() {
        lifecycleScope.launch {
            viewModel.listShows().collectLatest {
                showIndexAdapter?.submitData(it)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        showIndexAdapter = null
        _binding = null
    }
}