package com.gb.tvmaze.index.presentation.view.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.gb.tvmaze.index.R
import com.gb.tvmaze.index.databinding.ActivityMainBinding
import com.gb.tvmaze.index.presentation.util.KeyboardUtils
import com.gb.tvmaze.index.presentation.view.favorites.FavoritesFragment
import com.gb.tvmaze.index.presentation.view.index.ShowIndexFragment
import com.gb.tvmaze.index.presentation.view.search.SearchFragment
import com.gb.tvmaze.index.presentation.view.settings.SettingsFragment
import com.gb.tvmaze.index.presentation.view.show.ShowDetailsFragment
import com.gb.tvmaze.index.presentation.view.show.ShowDetailsFragment.Companion.ARGUMENT_SHOW_ID


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val fragmentListing = ShowIndexFragment()
    private val fragmentSearch = SearchFragment()
    private val fragmentFavorites = FavoritesFragment()
    private val fragmentSettings = SettingsFragment()
    private var fragmentShowDetails = ShowDetailsFragment()

    private val fm: FragmentManager = supportFragmentManager
    private var active: Fragment = fragmentListing

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupFragments()
        initBottomNavigation()

    }

    private fun setupFragments() {
        fm.beginTransaction()
            .add(R.id.fragment_container, fragmentSettings, SettingsFragment.TAG)
            .hide(fragmentSettings)
            .commit()
        
        fm.beginTransaction()
            .add(R.id.fragment_container, fragmentFavorites, FavoritesFragment.TAG)
            .hide(fragmentFavorites)
            .commit()
        
        fm.beginTransaction()
            .add(R.id.fragment_container, fragmentSearch, SearchFragment.TAG)
            .hide(fragmentSearch)
            .commit()
        
        fm.beginTransaction()
            .add(R.id.fragment_container,fragmentListing, ShowIndexFragment.TAG)
            .commit()
    }

    private fun initBottomNavigation() {
        with(binding) {
            bottomNavigation.setOnItemSelectedListener { item ->
                when(item.itemId) {
                    R.id.navigation_show_list -> {
                        changeCurrentFragment(fragmentListing); true
                    }
                    R.id.navigation_search -> {
                        changeCurrentFragment(fragmentSearch); true
                    }
                    R.id.navigation_favorites -> {
                        fragmentFavorites.getFavorites()
                        changeCurrentFragment(fragmentFavorites); true
                    }
                    R.id.navigation_settings -> {
                        changeCurrentFragment(fragmentSettings); true
                    }
                    else -> false
                }
            }
        }
    }

    private fun changeCurrentFragment(current: Fragment) {
        fm.findFragmentByTag(ShowDetailsFragment.TAG)?.let {
            if(it.isVisible) {
                fm.beginTransaction().remove(it).show(current).commit()
                active = current; return
            }
        }

        fm.beginTransaction().hide(active).show(current).commit()
        active = current
    }

    fun goToShowDetails(id: Int) {
        KeyboardUtils(this).hideKeyboard(binding.fragmentContainer)

        val bundle = Bundle().apply { this.putInt(ARGUMENT_SHOW_ID, id) }
        fragmentShowDetails = ShowDetailsFragment()
        fragmentShowDetails.arguments = bundle
            fm.beginTransaction()
                .add(
                    R.id.fragment_container,
                    fragmentShowDetails,
                    ShowDetailsFragment.TAG)
                .hide(fragmentShowDetails)
                .commit()
        fm.beginTransaction().hide(active).show(fragmentShowDetails).commit()
    }

    override fun onBackPressed() {
        fm.findFragmentByTag(ShowDetailsFragment.TAG)?.let {
            if(it.isVisible) {
                fragmentFavorites.getFavorites()
                fm.beginTransaction().remove(fragmentShowDetails).show(active).commit()
                return
            }
        }

        if(active != fragmentListing) {
            binding.bottomNavigation.menu
                .findItem(R.id.navigation_show_list)
                .isChecked = true
            fm.beginTransaction().hide(active).show(fragmentListing).commit()
            active = fragmentListing
            return
        }

        super.onBackPressed()
    }

}