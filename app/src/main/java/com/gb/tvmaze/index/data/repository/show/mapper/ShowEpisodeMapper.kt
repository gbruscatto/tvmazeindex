package com.gb.tvmaze.index.data.repository.show.mapper

import com.gb.tvmaze.index.core.entity.ShowEpisode
import com.gb.tvmaze.index.data.repository.show.data.ShowEpisodeData
import java.net.URL


fun ShowEpisodeData.toDomain() = ShowEpisode(
    id,
    name,
    summary ?: "",
    season,
    number,
    imagePreview?.medium.toURL(),
    rating?.average
)

private fun String?.toURL(): URL? =
    this?.let { runCatching { URL(it) }.getOrNull() }
